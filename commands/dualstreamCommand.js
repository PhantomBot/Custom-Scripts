(function () {
    var otherChannels = ($.inidb.exists('dualStreamCommand', 'otherChannels') ? $.inidb.get('dualStreamCommand', 'otherChannels') : null),
        timerToggle = ($.inidb.exists('dualStreamCommand', 'timerToggle') ? $.inidb.get('dualStreamCommand', 'timerToggle') : false),
        timerInterval = (parseInt($.inidb.exists('dualStreamCommand', 'timerInterval')) ? parseInt($.inidb.get('dualStreamCommand', 'timerInterval')) : 5);

    $.bind('command', function (event) {
        var sender = event.getSender(),
            command = event.getCommand(),
            args = event.getArgs(),
            argsString = event.getArguments().trim(),
            action = args[0],
            subAction = args[1];
    
        if (command.equalsIgnoreCase('multi')) {
            if (!action) {
                if (otherChannels != null) {
                    $.say('http://multistre.am/' + $.username.resolve($.channelName)  + otherChannels);
                    return;
                } else {
                    if ($.isModv3(sender, event.getTags())) {
                        $.say($.whisperPrefix(sender) + 'Usage: !multi set channel(s)');
                        return;
                    }
                }
            }

            if (action.equalsIgnoreCase('set')) {
                if (!$.isModv3(sender, event.getTags())) {
                    $.say($.whisperPrefix(sender) + $.modMsg);
                    return;
                } else if (!subAction) {
                    $.say($.whisperPrefix(sender) + 'Usage: !multi set channel(s)');
                    return;
                }
                argsString = argsString.replace('set', '');
                argsString = argsString.replace(' ', '/');
                otherChannels = argsString;
                $.inidb.set('dualStreamCommand', 'otherChannels', otherChannels);
                $.say('Multi link set! - http://multistre.am/' + $.username.resolve($.channelName) + otherChannels);
                return;
            }

            if (action.equalsIgnoreCase('clear')) {
                if (!$.isModv3(sender, event.getTags())) {
                    $.say($.whisperPrefix(sender) + $.modMsg);
                    return;
                }
                otherChannels = null;
                $.inidb.del('dualStreamCommand', 'otherChannels');
                $.say($.whisperPrefix(sender) + 'Multi command cleared, and disabled.');
                return;
            }

            if (action.equalsIgnoreCase('timer')) {
                if (!$.isModv3(sender, event.getTags())) {
                    $.say($.whisperPrefix(sender) + $.modMsg);
                    return;
                } else if (!subAction) {
                    $.say($.whisperPrefix(sender) + 'Usage: !multi timer [on / off]');
                    return;
                }

                if (subAction.equalsIgnoreCase('on')) {
                    timerToggle = true;
                    $.inidb.set('dualStreamCommand', 'timerToggle', timerToggle);
                    $.say($.whisperPrefix(sender) + 'Multi timer enabled.');
                    return;
                } else if (subAction.equalsIgnoreCase('off')) {
                    timerToggle = false;
                    $.inidb.set('dualStreamCommand', 'timerToggle', timerToggle);
                    $.say($.whisperPrefix(sender) + 'Multi timer disabled.');
                    return;
                } else {
                    $.say($.whisperPrefix(sender) + 'Usage: !multi timer [on / off]');
                    return;
                }
            }

            if (action.equalsIgnoreCase('timerinterval')) {
                if (!$.isModv3(sender, event.getTags())) {
                    $.say($.whisperPrefix(sender) + $.modMsg);
                    return;
                } else if (!subAction) {
                    $.say($.whisperPrefix(sender) + 'Usage: !multi timerinterval (time in minutes)');
                    return;
                } else if (parseInt(subAction) < 5) {
                    $.say($.whisperPrefix(sender) + 'Minimum for the timer is 5 minutes.');
                    return;
                }
                timerInterval = parseInt(subAction);
                $.inidb.set('dualStreamCommand', 'timerInterval', timerInterval);
                $.say($.whisperPrefix(sender) + 'Timer interval set!');
                return;
            }
        }
    });
    
    $.bind('initReady', function () {
        if ($.bot.isModuleEnabled('./commands/dualstreamCommand.js')) {
            $.registerChatCommand('./commands/dualstreamCommand.js', 'multi', 1);
            setInterval(function () {
                if (otherChannels != null) {
                    if (timerToggle) {
                        $.say('http://multistre.am/' + $.username.resolve($.channelName)  + otherChannels);
                        return;
                    }
                }
            }, timerInterval * 60 * 1000);
        }
    });
})();
