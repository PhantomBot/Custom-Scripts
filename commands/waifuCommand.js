$.waifuCommand = {
    getTotalWaifus: (parseInt($.inidb.GetKeyList('waifus', '').length) ? parseInt($.inidb.GetKeyList('waifus', '').length) : 0),
};

$.waifuCommand.unlockWaifu = function (event) {
    var sender = event.getSender();
    var r = parseInt($.inidb.GetKeyList('waifus', '').length);
    var List = parseInt($.inidb.GetKeyList(sender + '_list', '').length);
    var Random = $.randRange(0, r);
    var RandomWaifu = $.inidb.get('waifus', 'waifu_' + Random);
    var Filter = RandomWaifu.split('-').join('+').split(' ').join('+');
    var waifuAmount = (parseInt($.inidb.get(sender, 'waifu_ID_' + Random)) + 1);
	var unlockAmount = $.randRange(1,3);
	
    if (isNaN(waifuAmount)) {
        waifuAmount = 1;
    }

    if ($.inidb.exists(sender, 'waifu_ID_' + Random)) {
        $.inidb.incr(sender, 'waifu_ID_' + Random, unlockAmount);
        $.say($.getWhisperString(sender) + '[WAIFU GET!] you unlocked (+' + unlockAmount + ') more of ' + RandomWaifu + ' #' + Random + '. https://www.google.com/search?tbm=isch&q=' + Filter + '! You now have (' + waifuAmount + ')');
        return;
    } else {
        $.inidb.incr(sender, 'waifu_ID_' + Random, unlockAmount);
        $.inidb.incr(sender, 'waifus', 1);
        $.inidb.set(sender + '_list', List++, Random);
        $.say($.getWhisperString(sender) + '[WAIFU GET!] you unlocked a new waifu: (+' +  unlockAmount + ') of ' + RandomWaifu + ' #' + Random + '. https://www.google.com/search?tbm=isch&q=' + Filter);
        return;
    } 
};

$.waifuCommand.waifuRange = function (event, range) {
    var sender = event.getSender();
	var r = parseInt(range);
    var List = parseInt($.inidb.GetKeyList(sender + '_list', '').length);
    var Random = $.randRange(0, r);
    var RandomWaifu = $.inidb.get('waifus', 'waifu_' + Random);
    var Filter = RandomWaifu.split('-').join('+').split(' ').join('+');
    var waifuAmount = (parseInt($.inidb.get(sender, 'waifu_ID_' + Random)) + 1);
	var unlockAmount = $.randRange(1,3);
	
    if (isNaN(waifuAmount)) {
        waifuAmount = 1;
    }
	
	if (range < 100) {
		$.say("You can't enter a number lower than 100 for unpacking waifus.");
		return;
	}

    if ($.inidb.exists(sender, 'waifu_ID_' + Random)) {
        $.inidb.incr(sender, 'waifu_ID_' + Random, unlockAmount);
        $.say($.getWhisperString(sender) + '[WAIFU GET!] you unlocked (+' + unlockAmount + ') more of ' + RandomWaifu + ' #' + Random + '. https://www.google.com/search?tbm=isch&q=' + Filter + '! You now have (' + waifuAmount + ')');
        return;
    } else {
        $.inidb.incr(sender, 'waifu_ID_' + Random, unlockAmount);
        $.inidb.incr(sender, 'waifus', 1);
        $.inidb.set(sender + '_list', List++, Random);
        $.say($.getWhisperString(sender) + '[WAIFU GET!] you unlocked a new waifu: (+' +  unlockAmount + ') of ' + RandomWaifu + ' #' + Random + '. https://www.google.com/search?tbm=isch&q=' + Filter);
        return;
    } 
};

$.waifuCommand.randomWaifu = function (event) {
    var sender = event.getSender();
    var List = parseInt($.inidb.GetKeyList(sender + '_list', '').length);
    var Random = $.randRange(0, List);
    var RandomWaifu2 = $.inidb.get('waifus', 'waifu_' + $.inidb.get(sender + '_list', Random));
    
    if (!$.inidb.exists(sender, 'waifus') || $.inidb.get(sender, 'waifus') < 1) {
        $.say($.getWhisperString(sender) + 'You dont have a waifu! Start collecting waifus with !unlockwaifu');
        return;
    } else {
        if ($.inidb.exists('wowners', sender)) {
            RandomWaifu2 = $.inidb.get('wowners', sender);
            var Filter = RandomWaifu2.split('-').join('+').split(' ').join('+');
            $.say('/me ' + $.username.resolve(sender) + ', you are married to ' + RandomWaifu2 + ': https://www.google.com/search?tbm=isch&q=' + Filter);
            return; 
        } else {
            var Filter = RandomWaifu2.split('-').join('+').split(' ').join('+');
            $.say('/me ' + $.username.resolve(sender) + ', your waifu is ' + RandomWaifu2 + ' #' + $.inidb.get(sender + '_list', Random) + '. https://www.google.com/search?tbm=isch&q=' + Filter);
            return; 
        }
    }
};


$.waifuCommand.sendWaifu = function (event, receiver, waifu) {
    var sender = event.getSender();
    var args = event.getArgs();
    var List = parseInt($.inidb.GetKeyList(receiver + '_list', '').length);
    var Waifu = $.inidb.get('waifus', 'waifu_' + waifu);
    var waifuAmount = (parseInt($.inidb.get(sender, 'waifu_ID_' + waifu)) + 1);
    var Filter = Waifu.split('-').join('+').split(' ').join('+');
    var keys = $.inidb.GetKeyList(sender + '_list', '');

    if ($.inidb.get(sender, 'waifu_ID_' + waifu) >= 1) {
        for (var i = 0; keys.length; i++) {
            if ($.inidb.get(sender, 'waifu_ID_' + waifu) <= 1) {
                $.inidb.del(sender + '_list', i);
            }
            $.inidb.set(receiver + '_list', List++, waifu);
            $.inidb.decr(sender, 'waifu_ID_' + waifu, 1);
			$.inidb.decr(sender, 'waifus', 1);
			$.inidb.incr(receiver, 'waifu_ID_' + waifu, 1);
			$.inidb.incr(receiver, 'waifus', 1);
            $.say($.getWhisperString(sender) + 'you sent waifu: "' + Waifu + '" to ' + receiver + '. https://www.google.com/search?tbm=isch&q=' + Filter);
            $.say($.getWhisperString(receiver) + sender + ' sent you waifu: "' + Waifu + '". https://www.google.com/search?tbm=isch&q=' + Filter);
            return;
        }
    } else {
        $.say($.getWhisperString(sender) + 'you don\'t own that waifu.');
        return;
    }
};

$.waifuCommand.forceWaifu = function (event, waifu, receiver) {
    var sender = event.getSender();
    var List = parseInt($.inidb.GetKeyList(sender + '_list', '').length);
    var Waifu = $.inidb.get('waifus', 'waifu_' + waifu);
    var waifuAmount = parseInt($.inidb.get(receiver, 'waifu_ID_' + waifu)) + 1;
    var Filter = Waifu.split('-').join('+').split(' ').join('+');
    var receiver;

    if (isNaN(waifuAmount)) {
        waifuAmount = 1;
    }

    if (receiver == null) {
        receiver = sender;
    }
    if ($.inidb.exists(receiver, 'waifu_ID_' + waifu)) {
        $.inidb.incr(receiver, 'waifu_ID_' + waifu, 1);
        $.say($.getWhisperString(receiver) + '[WAIFU GET!] you now have (' + waifuAmount + ') of ' + Waifu + ' #' + waifu + '. https://www.google.com/search?tbm=isch&q=' + Filter + '!');
        return;
    } else {
        $.inidb.incr(receiver, 'waifu_ID_' + waifu, 1);
        $.inidb.incr(receiver, 'waifus', 1);
        $.inidb.set(receiver + '_list', List++, waifu);
        $.say($.getWhisperString(receiver) + '[WAIFU GET!] you unlocked a new waifu: ' + Waifu + ' #' + waifu + '. https://www.google.com/search?tbm=isch&q=' + Filter);
        return;
    } 
};

$.waifuCommand.getWaifu = function (event, waifu) {
    var sender = event.getSender();
    var Waifu = $.inidb.get('waifus', 'waifu_' + waifu);
    var waifuAmount = parseInt($.inidb.get(sender, 'waifu_ID_' + waifu));
    var Filter = Waifu.split('-').join('+').split(' ').join('+');
    
    if (isNaN(waifuAmount)) {
        waifuAmount = 0;
    }

    $.say($.getWhisperString(sender) + ', You have (' + waifuAmount + ') ' + Waifu + ' #' + waifu +  ': https://www.google.com/search?tbm=isch&q=' + Filter);
    return;
};

$.waifuCommand.checkWaifu = function (event) {
    var sender = event.getSender();
    var amount = $.inidb.get(sender, 'waifus');
    
    $.say($.getWhisperString(sender) + 'You have ' + amount + ' out of ' + $.waifuCommand.getTotalWaifus + ' waifus.');
    return;
};



$.bind('command', function (event) {
    var sender = event.getSender();
    var command = event.getCommand();
    var argsString = event.getArguments().trim();
    var args = event.getArgs();

    if (command.equalsIgnoreCase('waifu')) { 
        if (args.length == 0) {
            $.waifuCommand.randomWaifu(event);
            return;
        } else {
            $.waifuCommand.getWaifu(event, args[0]);
            return;
        }
    }

    
    if (command.equalsIgnoreCase('unlockwaifu')) { 
        $.waifuCommand.unlockWaifu(event);
        return;
    } 

	 if (command.equalsIgnoreCase('unpackwaifu')) { 
	 if (args.length < 1) {
		 $.say($.getWhisperString(sender) + 'Usage: !unpackwaifu (range) if you type 300 it will give you a waifu from 0 to 300');
		returnl
    } else {
		$.waifuCommand.waifuRange(event, args[0]);
        return;
	}
	 }

    if (command.equalsIgnoreCase('sendwaifu')) { 
        if (args.length < 2) {
            $.say($.getWhisperString(sender) + 'Usage: !sendwaifu (user) (waifu id)');
            return;
        }
        $.waifuCommand.sendWaifu(event, args[0], args[1]);
        return;
    }
    
    if (command.equalsIgnoreCase('forcewaifu')) { 
       if ($.isModv3(sender, event.getTags()) && args.length > 0 && $.inidb.exists('waifus', 'waifu_' + parseInt(args[0]))) {
            $.waifuCommand.forceWaifu(event, args[0], args[1]);
            return;
        } else if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else {
            $.say($.getWhisperString(sender) + 'Usage: !forcewaifu (waifu id)');
            return;
        }
    }
    
    if (command.equalsIgnoreCase('mywaifus')) { 
        $.waifuCommand.checkWaifu(event);
        return;
    } 
    
    if (command.equalsIgnoreCase('waifuhelp')) { 
        $.say($.getWhisperString(sender) + '!waifu - for a random waifu you own | !unlockwaifu - adds a random waifu to your list | '
        + '!setwaifu - sets a waifu on your list to default for when you use !waifu | '
        + '!waifu <id#> - checks for waifu data on that number and shows if you have it | !sendwaifu <id#> <person> - sends a waifu to specified target.');
        return;
    }

    if (command.equalsIgnoreCase('setwaifu')) {
        if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'You must specify a waifu id.');
            return;
        } else if (!$.inidb.exists('waifus', 'waifu_' + parseInt(args[0]))) {
            $.say($.getWhisperString(sender) + 'That waifu does not exist.');
            return;
        }
        if ($.inidb.exists(sender, 'waifu_ID_' + args[0])) {
            $.inidb.set('wowners', sender, $.inidb.get('waifus', 'waifu_' + parseInt(args[0])));
            $.say($.getWhisperString(sender) + 'You married ' + $.inidb.get('waifus', 'waifu_' + parseInt(args[0])));
            return; 
        } else {
            $.say($.getWhisperString(sender) + 'You dont have that waifu in your collection!');
            return;
        }
    }

    if (command.equalsIgnoreCase('splitwaifu')) { 
        if (!$.inidb.exists('wowners', sender)) {
            $.say($.getWhisperString(sender) + 'Your not married to any waifus.');
            return;
        }
        $.inidb.del('wowners', sender);
        $.say($.getWhisperString(sender) + 'You are now single!');
        return;
    }

  if (command.equalsIgnoreCase('addwaifu')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !addwaifu (waifu)');
            return;
        }
        $.waifuCommand.getTotalWaifus++;
        $.inidb.set('waifus', 'waifu_' + $.waifuCommand.getTotalWaifus, argsString);
        $.say($.getWhisperString(sender) + 'Waifu ' + argsString + ' has been added to the waifu list!');
        return;
    } else if (command.equalsIgnoreCase('delwaifu')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !delwaifu (waifu id)');
            return;
        } else if (!$.inidb.exists('waifus', 'waifu_' + parseInt(args[0]))) {
            $.say($.getWhisperString(sender) + 'That waifu does not exist!');
            return;
        }
        $.inidb.del('waifus', 'waifu_' + parseInt(args[0]));
        $.waifuCommand.getTotalWaifus--;
        $.say($.getWhisperString(sender) + 'Waifu #' + parseInt(args[0]) + ' was removed!');
        return;
    } else if (command.equalsIgnoreCase('editwaifu')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length < 2) {
            $.say($.getWhisperString(sender) + 'Usage: !editwaifu (waifu id) (waifu)');
            return;
        } else if (!$.inidb.exists('waifus', 'waifu_' + parseInt(args[0]))) {
            $.say($.getWhisperString(sender) + 'That waifu does not exist!');
            return;
        }
		var string = argsString.replace(args[0], '');
        $.inidb.set('waifus', 'waifu_' + parseInt(args[0]), string);
        $.say($.getWhisperString(sender) + 'Waifu #' + parseInt(args[0]) + ' has been edited!');
        return;
    }
});

if (!$.inidb.exists('settings', 'waifus_pushed')) {
    var waifu = new Array();
    
    waifu.push("Kanon Kanase -Strike the Blood-");
    waifu.push("Sayaka Kirasaka -Strike the Blood-");
    waifu.push("Reina Akatsuki -Strike the Blood-");
    waifu.push("Yukina Himeragi -Strike the Blood-");
    waifu.push("Yuuma Tokoyogi -Strike the Blood-");
    waifu.push("Asagi Aiba -Strike the Blood-");
    waifu.push("Holo -Spice & Wolf-");
    waifu.push("Norah Arendt -Spice & Wolf-");
    waifu.push("Chloe -Spice & Wolf-");
    waifu.push("Lan Fan -Fullmetal Alchemist-");
    waifu.push("Winry Rockbell -Fullmetal Alchemist-");
    waifu.push("Riza Hawkeye -Fullmetal Alchemist-");
    waifu.push("Olivier Mira Armstrong -Fullmetal Alchemist-");
    waifu.push("Lust -Fullmetal Alchemist-");
    waifu.push("Izumi Curtis -Fullmetal Alchemist-");
    waifu.push("May Chang -Fullmetal Alchemist-");
    waifu.push("Trisha Elric -Fullmetal Alchemist-");
    waifu.push("Sheska -Fullmetal Alchemist-");
    waifu.push("Elicia Hughes -Fullmetal Alchemist-");
    waifu.push("Pinako Rockbell -Fullmetal Alchemist-");
    waifu.push("Nina Tucker -Fullmetal Alchemist-");
    waifu.push("Maria Ross -Fullmetal Alchemist-");
    waifu.push("Rose Tomas -Fullmetal Alchemist-");
    waifu.push("Kathleen Elle Armstrong -Fullmetal Alchemist-");
    waifu.push("Paninya -Fullmetal Alchemist-");
    waifu.push("Rin Tohsaka -Fate/Stay Night-");
    waifu.push("Saber -Fate/Stay Night-");
    waifu.push("Illyasviel von Einzbern -Fate/Stay Night-");
    waifu.push("Rider -Fate/Stay Night-");
    waifu.push("Sakura Matou -Fate/Stay Night-");
    waifu.push("Caster -Fate/Stay Night-");
    waifu.push("Taiga Fujimura -Fate/Stay Night-");
    waifu.push("Ayako Mitsuzuri -Fate/Stay Night-");
    waifu.push("Sella -Fate/Stay Night-");
    waifu.push("Leysritt -Fate/Stay Night-");
    waifu.push("Yukika Saegusa -Fate/Stay Night-");
    waifu.push("Kaede Makidera -Fate/Stay Night-");
    waifu.push("Kane Himuro -Fate/Stay Night-");
    waifu.push("Otoko Hotaruzuka -Fate/Stay Night-");
    waifu.push("Levi Kazama -Trinity Seven-");
    waifu.push("Lieselotte Sherlock -Trinity Seven-");
    waifu.push("Akio Fudo -Trinity Seven-");
    waifu.push("Lilith Asami -Trinity Seven-");
    waifu.push("Yui Kurata -Trinity Seven-");
    waifu.push("Mira Yamana -Trinity Seven-");
    waifu.push("Hijiri Kasuga -Trinity Seven-");
    waifu.push("Selina Sherlock -Trinity Seven-");
    waifu.push("Astil Manuscript -Trinity Seven-");
    waifu.push("Arin Kannazuki -Trinity Seven-");
    waifu.push("Shana -Shakugan no Shana-");
    waifu.push("Kazumi Yoshida -Shakugan no Shana-");
    waifu.push("Ogata Matake -Shakugan no Shana-");
    waifu.push("Fumina Konoe -Shakugan no Shana-");
    waifu.push("Wilhelmina Carmel -Shakugan no Shana-");
    waifu.push("Margery Daw -Shakugan no Shana-");
    waifu.push("Mathilde Saint-Omer -Shakugan no Shana-");
    waifu.push("WestShore -Shakugan no Shana-");
    waifu.push("Hecate -Shakugan no Shana-");
    waifu.push("Pheles -Shakugan no Shana-");
    waifu.push("Rise Kujikawa -Shin Megami Tensei: Persona 4-");
    waifu.push("Yukiko Amagi -Shin Megami Tensei: Persona 4-");
    waifu.push("Chie Satonaka -Shin Megami Tensei: Persona 4-");
    waifu.push("Margeret -Shin Megami Tensei: Persona 4-");
    waifu.push("Naoto Shirogane -Shin Megami Tensei: Persona 4-");
    waifu.push("Marie -Shin Megami Tensei: Persona 4-");
    waifu.push("Hanako Ohtani -Shin Megami Tensei: Persona 4-");
    waifu.push("Noriko Kashiwagi -Shin Megami Tensei: Persona 4-");
    waifu.push("Ai Ebihara -Shin Megami Tensei: Persona 4-");
    waifu.push("Saki Konishi -Shin Megami Tensei: Persona 4-");
    waifu.push("Yumi Ozawa -Shin Megami Tensei: Persona 4-");
    waifu.push("Sayoko Uehara -Shin Megami Tensei: Persona 4-");
    waifu.push("Aigis -Shin Megami Tensei: Persona 3-");
    waifu.push("Elizabeth -Shin Megami Tensei: Persona 3-");
    waifu.push("Yukari Takeba -Shin Megami Tensei: Persona 3-");
    waifu.push("Female Protagonist -Shin Megami Tensei: Persona 3-");
    waifu.push("Fuuka Yamagishi -Shin Megami Tensei: Persona 3-");
    waifu.push("Mitsuru Kirijo -Shin Megami Tensei: Persona 3-");
    waifu.push("Chidori Yoshino -Shin Megami Tensei: Persona 3-");
    waifu.push("Chizuru Minamoto -Kanokon-");
    waifu.push("Nozomu Ezomori -Kanokon-");
    waifu.push("Ren Nanao -Kanokon-");
    waifu.push("Yukihana -Kanokon-");
    waifu.push("Ai Nanao -Kanokon-");
    waifu.push("Akane Asahina -Kanokon-");
    waifu.push("Tamamo -Kanokon-");
    waifu.push("Mio Osakabe -Kanokon-");
    waifu.push("Iku Sahara -Kanokon-");
    waifu.push("Kiriko Takana -Kanokon-");
    waifu.push("Minori Mitama -Kanokon-");
    waifu.push("Nue -Kanokon-");
    waifu.push("Minato Tomoka -Ro-Kyu-Bu!-");
    waifu.push("Hinata Hakamada -Ro-Kyu-Bu!-");
    waifu.push("Maho Misawa -Ro-Kyu-Bu!-");
    waifu.push("Saki Nagatsuka -Ro-Kyu-Bu!-");
    waifu.push("Airi Kashii -Ro-Kyu-Bu!-");
    waifu.push("Mihoshi Takamura -Ro-Kyu-Bu!-");
    waifu.push("Aoi Ogiyama -Ro-Kyu-Bu!-");
    waifu.push("Satsuki Kakizono  -Ro-Kyu-Bu!-");
    waifu.push("Miyu Aida -Ro-Kyu-Bu!-");
    waifu.push("Nayu Hasegawa -Ro-Kyu-Bu!-");
    waifu.push("Touko Hatano -Ro-Kyu-Bu!-");
    waifu.push("Tae Mishouji  -Ro-Kyu-Bu!-");
    waifu.push("Kagetsu Hakamada -Ro-Kyu-Bu!-");
    waifu.push("Hijiri Kuina -Ro-Kyu-Bu!-");
    waifu.push("Manaka Nobidome -Ro-Kyu-Bu!-");
    waifu.push("Hatsue Nobidome -Ro-Kyu-Bu!-");
    waifu.push("Yuuki Asuna -Sword Art Online-");
    waifu.push('Konno Yuuki -Sword Art Online-');
    waifu.push("Silica -Sword Art Online-");
    waifu.push("Lisbeth -Sword Art Online-");
    waifu.push("Yui -Sword Art Online-");
    waifu.push("Sinon -Sword Art Online-");
    waifu.push("Kirigaya Suguha -Sword Art Online-");
    waifu.push("Alice Schuberg -Sword Art Online-");
    waifu.push("Sachi -Sword Art Online-");
    waifu.push("Yalicia Rue -Sword Art Online-");
    waifu.push("Yuuki Kyouko -Sword Art Online-");
    waifu.push("Sasha -Sword Art Online-");
    waifu.push("Sakuya -Sword Art Online-");
    waifu.push("Yulier -Sword Art Online-");
    waifu.push("Siune -Sword Art Online-");
    waifu.push("Rosalia -Sword Art Online-");
    waifu.push("Griselda -Sword Art Online-");
    waifu.push("Tieze Shtolienen -Sword Art Online-");
    waifu.push("Skuld -Sword Art Online-");
    waifu.push("Uror -Sword Art Online-");
    waifu.push("Verdandi -Sword Art Online-");
    waifu.push("Yuriko Aoki -Bakuman-");
    waifu.push("Kaya Miyoshi -Bakuman-");
    waifu.push("Miho Azuki -Bakuman-");
    waifu.push("Aiko Iwase -Bakuman-");
    waifu.push("Miyuki Azuki -Bakuman-");
    waifu.push("Mina Azuki -Bakuman-");
    waifu.push("Kirino Kousaka -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Ruri Gokou -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Ayase Aragaki -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Manami Tamura -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Saori Makishima -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Kanako Kurusu -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Bridget Evans -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Sena Akagi -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Hinata Gokou -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Yoshino Kousaka -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Tamaki Gokou -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Ria Hagry -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Kirara Hoshino -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Kanata Kurusu -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Kaori Makishima -Ore no Imouto ga Konnani Kawaii Wake ga Nai-");
    waifu.push("Satsuki Kiryuin -Kill la Kill-");
    waifu.push("Ryuko Matoi -Kill la Kill-");
    waifu.push("Mako Mankanshoku -Kill la Kill-");
    waifu.push("Minori Kushieda -Toradora!-");
    waifu.push("Taiga Aisaka -Toradora!-");
    waifu.push("Sumire Kano -Toradora!-");
    waifu.push("Yasuko Takasu -Toradora!-");
    waifu.push("Beatrice Ushiromiya  -Umineko no Naku Koro ni-");
    waifu.push("Shannon -Umineko no Naku Koro ni-");
    waifu.push("Asia Argento -High School DxD-");
    waifu.push("Koneko Toujou -High School DxD-");
    waifu.push("Akeno Himejima -High School DxD-");
    waifu.push("Irina Shido -High School DxD-");
    waifu.push("Xenovia Quarta -High School DxD-");
    waifu.push("Rias Gremory -High School DxD-");
    waifu.push("Black Rose -.hack-");
    waifu.push("Hestia -DanMachi-");
    waifu.push("Aiz Wallenstein -DanMachi-");
    waifu.push("Liliruca Arde -DanMachi-");
    waifu.push("Ryu Lion -DanMachi-");
    waifu.push("Loki -DanMachi-");
    waifu.push("Syr Flover -DanMachi-");
    waifu.push("Freya -DanMachi-");
    waifu.push("Eina Tulle -DanMachi-");
    waifu.push("Hephaistios -DanMachi-");
    waifu.push("Asufi Al Andromeda -DanMachi-");
    waifu.push("Anya Flomer -DanMachi-");
    waifu.push("Chloe Lolo -DanMachi-");
    waifu.push("Tiona Hiryute -DanMachi-");
    waifu.push("Tione Hiryute -DanMachi-");
    waifu.push("Mikoto Yamato -DanMachi-");
    waifu.push("Naza Ersuisu -DanMachi-");
    waifu.push("Demeter -DanMachi-");
    waifu.push("Lefiya Viridis -DanMachi-");
    waifu.push("Moeka Kiryu -Steins;Gate-");
    waifu.push("Mayuri Shiina -Steins;Gate-");
    waifu.push("Suzuha Amane -Steins;Gate-");
    waifu.push("Faris Nyannyan -Steins;Gate-");
    waifu.push("Makise Kurisu -Steins;Gate-");
    waifu.push("Nagisa Furukawa -CLANNAD-");
    waifu.push("Tomoyo Sakagami -CLANNAD-");
    waifu.push("Kyou Fujibayashi -CLANNAD-");
    waifu.push("Fuuko Ibuki -CLANNAD-");
    waifu.push("Kotomi Ichinose -CLANNAD-");
    waifu.push("Ryou Fujibayashi -CLANNAD-");
    waifu.push("Sanae Furukawa -CLANNAD-");
    waifu.push("Yukine Miyazawa -CLANNAD-");
    waifu.push("Mei Sunohara -CLANNAD-");
    waifu.push("Misae Sagara -CLANNAD-");
    waifu.push("Kouko Ibuki -CLANNAD-");
    waifu.push("Rie Nishina -CLANNAD-");
    waifu.push("Kouko Kaga -Golden Time-");
    waifu.push("Nana Hayashida -Golden Time-");
    waifu.push("Nana -Golden Time-");
    waifu.push("Chinami Oka -Golden Time-");
    waifu.push("Sao -Golden Time-");
    waifu.push("Reina -Golden Time-");
    waifu.push("Kallen Stadtfeld -Code Geass-");
    waifu.push("C.C. -Code Geass-");
    waifu.push("Shirley Fenette -Code Geass-");
    waifu.push("Anya Alstreim -Code Geass-");
    waifu.push("Nunnally Lamperouge -Code Geass-");
    waifu.push("Cornelia li Britannia -Code Geass-");
    waifu.push("Villetta Nu -Code Geass-");
    waifu.push("Milly Ashford -Code Geass-");
    waifu.push("Kaguya Sumeragi -Code Geass-");
    waifu.push("Rakshata Chawla -Code Geass-");
    waifu.push("Jiang Lihua -Code Geass-");
    waifu.push("Nina Einstein -Code Geass-");
    waifu.push("V.V. -Code Geass-");
    waifu.push("Sayoko Shinozaki -Code Geass-");
    waifu.push("Cecile Croomy -Code Geass-");
    waifu.push("Marianne vi Britannia -Code Geass-");
    waifu.push("Liliana Vergamon -Code Geass-");
    waifu.push("Miya I. Hillmick -Code Geass-");
    waifu.push("Ayame Futaba  -Code Geass-");
    waifu.push("Carine ne Britannia -Code Geass-");
    waifu.push("Nonette Enneagram -Code Geass-");
    waifu.push("Euphemia li Britannia -Code Geass-");
    waifu.push("Yoko Littner -Gurren Lagann-");
    waifu.push("Nia Teppelin -Gurren Lagann-");
    waifu.push("Kiyal Bachika -Gurren Lagann-");
    waifu.push("Adiane -Gurren Lagann-");
    waifu.push("Darry Adai -Gurren Lagann-");
    waifu.push("Kinon Bachika -Gurren Lagann-");
    waifu.push("Kiyoh Littner  -Gurren Lagann-");
    waifu.push("Saeko Busujima -The High School of the Dead-");
    waifu.push("Rei Miyamoto -The High School of the Dead-");
    waifu.push("Saya Takagi -The High School of the Dead-");
    waifu.push("Shizuka Marikawa -The High School of the Dead-");
    waifu.push("Alice Maresato -The High School of the Dead-");
    waifu.push("Rika Minami -The High School of the Dead-");
    waifu.push("Yuriko Takagi -The High School of the Dead-");
    waifu.push("Miku Yuuki -The High School of the Dead-");
    waifu.push("Toshimi Niki -The High School of the Dead-");
    waifu.push("Misuzu Ichijou -The High School of the Dead-");
    waifu.push("Erza Scarlet -Fairy Tail-");
    waifu.push("Lucy Heartfilia -Fairy Tail-");
    waifu.push("Wendy Marvell -Fairy Tail-");
    waifu.push("Juvia Lockser -Fairy Tail-");
    waifu.push("Mirajane Strauss -Fairy Tail-");
    waifu.push("Cana Alberona -Fairy Tail-");
    waifu.push("Lisanna Strauss -Fairy Tail-");
    waifu.push("Levy McGarden -Fairy Tail-");
    waifu.push("Chico C Hammitt -Fairy Tail-");
    waifu.push("Mavis Vermilion -Fairy Tail-");
    waifu.push("Sherry Blendy -Fairy Tail-");
    waifu.push("Sherria Blendy -Fairy Tail-");
    waifu.push("Yukino Agria -Fairy Tail-");
    waifu.push("Flare Corona -Fairy Tail-");
    waifu.push("Minerva Orland -Fairy Tail-");
    waifu.push("Chiyo Sakura -Gekkan Shoujo Nozaki-kun-");
    waifu.push("Yuzuki Seo -Gekkan Shoujo Nozaki-kun-");
    waifu.push("Mamiko -Gekkan Shoujo Nozaki-kun-");
    waifu.push("Yuu Kashima -Gekkan Shoujo Nozaki-kun-");
    waifu.push("Mai Minakami -Nichijou-");
    waifu.push("Mio Naganohara -Nichijou-");
    waifu.push("Nano Shinonome -Nichijou-");
    waifu.push("Yuuko Aioi -Nichijou-");
    waifu.push("Chiho Sasaki -The Devil is a Part-Timer!-");
    waifu.push("Emi Yusa -The Devil is a Part-Timer!-");
    waifu.push("Emeralda Etuva -The Devil is a Part-Timer!-");
    waifu.push("Suzuno Kamazuki -The Devil is a Part-Timer!-");
    waifu.push("Mihoshi -Gundam Build Fighters-");
    waifu.push("Rinko Iori -Gundam Build Fighters-");
    waifu.push("Aila Jyrkiäinen -Gundam Build Fighters-");
    waifu.push("China Kousaka -Gundam Build Fighters-");
    waifu.push("Fumina Hoshino -Gundam Build Fighters Try-");
    waifu.push("Mirai Kamiki -Gundam Build Fighters Try-");
    waifu.push("Shia Kijima -Gundam Build Fighters Try-");
    waifu.push("Kaoruko Sazaki -Gundam Build Fighters Try-");
    waifu.push("Anri Sonohara -Durarara!!-");
    waifu.push("Celty Sturluson -Durarara!!-");
    waifu.push("Ruri Hijiribe -Durarara!!-");
    waifu.push("Erika Karisawa -Durarara!!-");
    waifu.push("Namie Yagiri -Durarara!!-");
    waifu.push("Saki Mikajima -Durarara!!-");
    waifu.push("Mika Harima -Durarara!!-");
    waifu.push("Haruna Niekawa -Durarara!!-");
    waifu.push("Ruri Hijiribe -Durarara!!-");
    waifu.push("Rio Kamichika -Durarara!!-");
    waifu.push("Kururi Orihara -Durarara!!-");
    waifu.push("Mairu Orihara -Durarara!!-");
    waifu.push("Vorona -Durarara!!-");
    waifu.push("Akane Awakusu -Durarara!!-");
    waifu.push("Emilia Kishitani -Durarara!!-");
    waifu.push("Index Librorum Prohibitorum -Toaru Majutsu no Index-");
    waifu.push("Aisa Himegami -Toaru Majutsu no Index-");
    waifu.push("Kaori Kanzaki -Toaru Majutsu no Index-");
    waifu.push("Mikoto Misaka -Toaru Majutsu no Index-");
    waifu.push("Kuroko Shirai -Toaru Majutsu no Index-");
    waifu.push("Bulma -Dragon Ball-");
    waifu.push("Launch -Dragon Ball-");
    waifu.push("Chi Chi -Dragon Ball Z-");
    waifu.push("18 -Dragon Ball Z-");
    waifu.push("Filia -Skullgirls-");
    waifu.push("Cerebella -Skullgirls-");
    waifu.push("Peacock -Skullgirls-");
    waifu.push("Parasoul -Skullgirls-");
    waifu.push("Ms.Fortune -Skullgirls-");
    waifu.push("Painwheel -Skullgirls-");
    waifu.push("Valentine -Skullgirls-");
    waifu.push("Double -Skullgirls-");
    waifu.push("Squigly -Skullgirls-");
    waifu.push("Eliza -Skullgirls-");
    waifu.push("Robo-Fortune -Skullgirls-");
    waifu.push("Celica A. Mercury -Blazblue-");
    waifu.push("Noel Vermillion -Blazblue-");
    waifu.push("Lambda -No. 11- -Blazblue-");
    waifu.push("Kokonoe Mercury -Blazblue-");
    waifu.push("Taokaka -Blazblue-");
    waifu.push("Konoe A. Mercury -Blazblue-");
    waifu.push("Nu -No. 13- -Blazblue-");
    waifu.push("Rachel Alucard -Blazblue-");
    waifu.push("Makoto Nanaya -Blazblue-");
    waifu.push("Mu -No. 12- -Blazblue-");
    waifu.push("Bullet -Blazblue-");
    waifu.push("Litchi Faye-Ling -Blazblue-");
    waifu.push("Platinum the Trinity -Blazblue-");
    waifu.push("Saya -Blazblue-");
    waifu.push("Tsubaki Yayaoi -Blazblue-");
    waifu.push("Hades Izanami -Blazblue-");
    waifu.push("Mai Natsume -Blazblue-");
    waifu.push("Izayoi -Blazblue-");
    waifu.push("Trinity Glassfille -Blazblue-");
    waifu.push("Ex Machina: Minerva -Blazblue-");
    waifu.push("Torakaka -Blazblue-");
    waifu.push("Ada Clover -Blazblue-");
    waifu.push("Kajun Faycott -Blazblue-");
    waifu.push("Ms. Tail -Blazblue-");
    waifu.push("Chachakaka -Blazblue-");
    waifu.push("Linne -UNDER NIGHT IN-BIRTH-");
    waifu.push("Vatista -UNDER NIGHT IN-BIRTH-");
    waifu.push("Yuzuriha -UNDER NIGHT IN-BIRTH-");
    waifu.push("Hilda -UNDER NIGHT IN-BIRTH-");
    waifu.push("Eltnum -UNDER NIGHT IN-BIRTH-");
    waifu.push("Nanase -UNDER NIGHT IN-BIRTH-");
    waifu.push("Orie -UNDER NIGHT IN-BIRTH-");
    waifu.push("Phonon -UNDER NIGHT IN-BIRTH-");
    waifu.push("Dizzy -Guilty Gear-");
    waifu.push("Elphelt Valentine -Guilty Gear-");
    waifu.push("Ramlethal Valentine -Guilty Gear-");
    waifu.push("I-No -Guilty Gear-");
    waifu.push("Millia Rage -Guilty Gear-");
    waifu.push("May -Guilty Gear-");
    waifu.push("Justice -Guilty Gear-");
    waifu.push("Valentine -Guilty Gear-");
    waifu.push("Baiken -Guilty Gear-");
    waifu.push("A.B.A -Guilty Gear-");
    waifu.push("Jam Kuradoberi -Guilty Gear-");
    waifu.push("Aria -Guilty Gear-");
    waifu.push("Mari Kurihara -Prison School-");
    waifu.push("Meiko Shiraki -Prison School-");
    waifu.push("Hana Midorikawa -Prison School-");
    waifu.push("Chiyo Kurihara -Prison School-");
    waifu.push("Kate Takenomiya -Prison School-");
    waifu.push("Risa Bettou -Prison School-");
    waifu.push("Anzu Yokoyama -Prison School-");
    waifu.push("Mitsuko Yokoyama -Prison School-");
    waifu.push("Tachibana Kanade -Angel Beats-");
    waifu.push("Nakamura Yuri -Angel Beats-");
    waifu.push("Shiina -Angel Beats-");
    waifu.push("Yui -Angel Beats-");
    waifu.push("Masami Iwasawa -Angel Beats-");
    waifu.push("Hatsune Otonashi -Angel Beats-");
    waifu.push("Shiori Sekine  -Angel Beats-");
    waifu.push("Miyuki Irie -Angel Beats-");
    waifu.push("Hisako -Angel Beats-");
    waifu.push("Kuroyukihime -Accel World-");
    waifu.push("Chiyuri Kurashima -Accel World-");
    waifu.push("Yuniko Kouzuki -Accel World-");
    waifu.push("Fuuko Kurasaki -Accel World-");
    waifu.push("Blood Leopard -Accel World-");
    waifu.push("Megumi Wakamiya -Accel World-");
    waifu.push("Aqua Current -Accel World-");
    waifu.push("Mana Itosu -Accel World-");
    waifu.push("Nickel Doll -Accel World-");
    waifu.push("Ruka Asato  -Accel World-");
    waifu.push("Yukino Yukinoshita -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Yui Yuigahama -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Komachi Hikigaya  -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Shizuka Hiratsuka -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Haruno Yukinoshita -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Saki Kawasaki -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Rumi Tsurumi -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Yumiko Miura -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Hina Ebina -Yahari Ore no Seishun Love Comedy wa Machigatteiru-");
    waifu.push("Yui Hirasawa -K-On!-");
    waifu.push("Mio Akiyama -K-On!-");
    waifu.push("Azusa Nakano -K-On!-");
    waifu.push("Tsumugi Kotobuki -K-On!-");
    waifu.push("Ritsu Tainaka -K-On!-");
    waifu.push("Juri Han -Street Fighter-");
    waifu.push("Decapre -Street Fighter-");
    waifu.push("Cammy White -Street Fighter-");
    waifu.push("Elena -Street Fighter-");
    waifu.push("Chun-Li -Street Fighter-");
    waifu.push("Poison -Street Fighter-");
    waifu.push("Sakura -Street Fighter-");
    waifu.push("Ibuki -Street Fighter-");
    waifu.push("Makoto -Street Fighter-");
    waifu.push("Rose -Street Fighter-");
    waifu.push("Crimson Viper -Street Fighter-");
    waifu.push("Karin Kanzuki -Street Fighter-");
    waifu.push("Juli -Street Fighter-");
    waifu.push("Juni -Street Fighter-");
    waifu.push("Rainbow Mika -Street Fighter-");
    waifu.push("Ingrid -Street Fighter-");
    waifu.push("Eliza Masters -Street Fighter-");
    waifu.push("Ayame Kajou -Shimoneta-");
    waifu.push("Otome Saotome -Shimoneta-");
    waifu.push("Kosuri Onigashira -Shimoneta-");
    waifu.push("Anna Nishikinomiya -Shimoneta-");
    waifu.push("Hyouka Fuwa -Shimoneta-");
    waifu.push("Oboro Tsukimigusa -Shimoneta-");
    waifu.push("Youko Shiragami -Jitsu wa Watashi wa-");
    waifu.push("Akane Koumoto -Jitsu wa Watashi wa-");
    waifu.push("Nagisa Aizawa -Jitsu wa Watashi wa-");
    waifu.push("Mikan Akemi -Jitsu wa Watashi wa-");
    waifu.push("Shiho Shishido -Jitsu wa Watashi wa-");
    waifu.push("Akari Koumoto -Jitsu wa Watashi wa-");
    waifu.push("Mizore Shirayuki -Rosario to Vampire-");
    waifu.push("Moka Akashiya -Rosario to Vampire-");
    waifu.push("Kurumu Kurono -Rosario to Vampire-");
    waifu.push("Yukari Sendou -Rosario to Vampire-");
    waifu.push("Ruby Toujo -Rosario to Vampire-");
    waifu.push("Shizuka Nekonome -Rosario to Vampire-");
    waifu.push("Ririko Kagome -Rosario to Vampire-");
    waifu.push("Kasumi Aono -Rosario to Vampire-");
    waifu.push("Deshiko Deshi -Rosario to Vampire-");
    waifu.push("Tamao Ichinose -Rosario to Vampire-");
    waifu.push("Akatsuki -Log Horizon-");
    waifu.push("Rayneshia El-Arte Corwen -Log Horizon-");
    waifu.push("Serara -Log Horizon-");
    waifu.push("Minori -Log Horizon-");
    waifu.push("Kanami -Log Horizon-");
    waifu.push("Marielle -Log Horizon-");
    waifu.push("Isuzu -Log Horizon-");
    waifu.push("Henrietta -Log Horizon-");
    waifu.push("Nureha -Log Horizon-");
    waifu.push("Misa Takayama -Log Horizon-");
    waifu.push("Nazuna -Log Horizon-");
    waifu.push("Liliana -Log Horizon-");
    waifu.push("Isami -Log Horizon-");
    waifu.push("Roe 2 -Log Horizon-");
    waifu.push("Mariandale -Ixion Saga DT-");
    waifu.push("Ecarlate Juptris St. Piria -Ixion Saga DT-");
    waifu.push("Miranda -Ixion Saga DT-");
    waifu.push("Almaflora -Ixion Saga DT-");
    waifu.push("Emilia -Ixion Saga DT-");
    waifu.push("Chaika Trabant -Hitsugi no Chaika-");
    waifu.push("Akari Acura -Hitsugi no Chaika-");
    waifu.push("Fredrica -Hitsugi no Chaika-");
    waifu.push("Chaika Bohdan -Hitsugi no Chaika-");
    waifu.push("Viivi Holopainen -Hitsugi no Chaika-");
    waifu.push("Layla -Hitsugi no Chaika-");
    waifu.push("Dominica Skoda -Hitsugi no Chaika-");
    waifu.push("Zita Brusasco -Hitsugi no Chaika-");
    waifu.push("Selma Kenworth -Hitsugi no Chaika-");
    waifu.push("Julia -Hitsugi no Chaika-");
    waifu.push("Karen Bombardier -Hitsugi no Chaika-");
    waifu.push("Hasumin Orlo -Hitsugi no Chaika-");
    waifu.push("Chaika Kamaz -Hitsugi no Chaika-");
    waifu.push("Niva Lada -Hitsugi no Chaika-");
    waifu.push("Claudia Dodge -Hitsugi no Chaika-");
    waifu.push("Alina Hartgen -Hitsugi no Chaika-");
    waifu.push("Irina Hartgen -Hitsugi no Chaika-");
    waifu.push("Ursula Tatra 12 -Hitsugi no Chaika-");
    waifu.push("Eureka -Eureka Seven-");
    waifu.push("Talho Yuuki -Eureka Seven-");
    waifu.push("Anemone -Eureka Seven-");
    waifu.push("Ray Beams -Eureka Seven-");
    waifu.push("Gidget -Eureka Seven-");
    waifu.push("Sakuya -Eureka Seven-");
    waifu.push("Hilda -Eureka Seven-");
    waifu.push("Mischa -Eureka Seven-");
    waifu.push("Diane Thurston -Eureka Seven-");
    waifu.push("Sonia Wakabayashi -Eureka Seven-");
    waifu.push("Tiptory -Eureka Seven-");
    waifu.push("Martha -Eureka Seven-");
    waifu.push("Coda Lovell -Eureka Seven-");
    waifu.push("Maki Nishikino -Love Live-");
    waifu.push("Niko Yazawa -Love Live-");
    waifu.push("Kotori Minami -Love Live-");
    waifu.push("Nozomi Toujou -Love Live-");
    waifu.push("Umi Sonoda -Love Live-");
    waifu.push("Honoka Kousaka -Love Live-");
    waifu.push("Eri Ayase -Love Live-");
    waifu.push("Rin Hoshizora -Love Live-");
    waifu.push("Hanayo Koizumi -Love Live-");
    waifu.push("Tsubasa Kira -Love Live-");
    waifu.push("Chihaya Kisaragi -The iDOLM@STER-");
    waifu.push("Miki Hoshii -The iDOLM@STER-");
    waifu.push("Makoto Kikuchi -The iDOLM@STER-");
    waifu.push("Takane Shijou -The iDOLM@STER-");
    waifu.push("Hibiki Ganaha -The iDOLM@STER-");
    waifu.push("Iori Minase -The iDOLM@STER-");
    waifu.push("Haruka Amami -The iDOLM@STER-");
    waifu.push("Yayoi Takatsuki -The iDOLM@STER-");
    waifu.push("Yukiho Hagiwara -The iDOLM@STER-");
    waifu.push("Azusa Miura -The iDOLM@STER-");
    waifu.push("Mami Futami -The iDOLM@STER-");
    waifu.push("Ritsuko Akizuki -The iDOLM@STER-");
    waifu.push("Ami Futami -The iDOLM@STER-");
    waifu.push("Mami Futami -The iDOLM@STER-");
    waifu.push("Maki Kamii -Idol*Sister-");
    waifu.push("Ayaka Takano -Idol*Sister-");
    waifu.push("Brother Takano -Idol*Sister-");
    waifu.push("Shiro -No Game No Life-");
    waifu.push("Stephanie Dora -No Game No Life-");
    waifu.push("Jibril -No Game No Life-");
    waifu.push("Izuna Hatsuse -No Game No Life-");
    waifu.push("Chlammy Zell -No Game No Life-");
    waifu.push("Feel Nilvalen -No Game No Life-");
    waifu.push("Miko -No Game No Life-");
    waifu.push("Queen -No Game No Life-");
    waifu.push("Miia -Monster Musume no Iru Nichijou-");
    waifu.push("Rachnera Arachnera -Monster Musume no Iru Nichijou-");
    waifu.push("Papi -Monster Musume no Iru Nichijou-");
    waifu.push("Suu -Monster Musume no Iru Nichijou-");
    waifu.push("Centorea Shianus -Monster Musume no Iru Nichijou-");
    waifu.push("Meroune Lorelei  -Monster Musume no Iru Nichijou-");
    waifu.push("Zombina -Monster Musume no Iru Nichijou-");
    waifu.push("Lala -Monster Musume no Iru Nichijou-");
    waifu.push("Doppel -Monster Musume no Iru Nichijou-");
    waifu.push("Manako -Monster Musume no Iru Nichijou-");
    waifu.push("Sumisu -Monster Musume no Iru Nichijou-");
    waifu.push("Himeko Inaba -Kokoro Connect-");
    waifu.push("Iori Nagase -Kokoro Connect-");
    waifu.push("Yui Kiriyama -Kokoro Connect-");
    waifu.push("Cthuko -Haiyore Nyaruko-san-");
    waifu.push("Nyaruko -Haiyore Nyaruko-san-");
    waifu.push("Hasuta -Haiyore Nyaruko-san-");
    waifu.push("Luhy Jistone -Haiyore Nyaruko-san-");
    waifu.push("Ghutatan -Haiyore Nyaruko-san-");
    waifu.push("Tamao Kurei -Haiyore Nyaruko-san-");
    waifu.push("Miyuki Shiba -The Irregular at Magic High School-");
    waifu.push("Mayumi Saegusa -The Irregular at Magic High School-");
    waifu.push("Erika Chiba -The Irregular at Magic High School-");
    waifu.push("Shizuku Kitayama -The Irregular at Magic High School-");
    waifu.push("Honoka Mitsui -The Irregular at Magic High School-");
    waifu.push("Mari Watanabe -The Irregular at Magic High School-");
    waifu.push("Mizuki Shibata -The Irregular at Magic High School-");
    waifu.push("Kyouko Fujibayashi -The Irregular at Magic High School-");
    waifu.push("Azusa Nakajou -The Irregular at Magic High School-");
    waifu.push("Haruka Ono -The Irregular at Magic High School-");
    waifu.push("Sayaka Mibu -The Irregular at Magic High School-");
    waifu.push("Kanon Chiyoda -The Irregular at Magic High School-");
    waifu.push("Maya Yotsuba -The Irregular at Magic High School-");
    waifu.push("Amelia Eimi Akechi Goldie -The Irregular at Magic High School-");
    waifu.push("Izana Shinatose -Knights of Sidonia-");
    waifu.push("Tsumugi Shiraui -Knights of Sidonia-");
    waifu.push("Benisuzume -Knights of Sidonia-");
    waifu.push("Kobayashi -Knights of Sidonia-");
    waifu.push("Lala Hiyama -Knights of Sidonia-");
    waifu.push("Yuhata Midorikawa -Knights of Sidonia-");
    waifu.push("Samari Ittan -Knights of Sidonia-");
    waifu.push("Sasaki -Knights of Sidonia-");
    waifu.push("Numi Tahiro -Knights of Sidonia-");
    waifu.push("Ren Honoka -Knights of Sidonia-");
    waifu.push("Yure Shinatose -Knights of Sidonia-");
    waifu.push("En Honoka -Knights of Sidonia-");
    waifu.push("Mozuku Kunato -Knights of Sidonia-");
    waifu.push("Shizuka Hoshijiro -Knights of Sidonia-");
    waifu.push("Albedo -Overlord-");
    waifu.push("Shalltear Bloodfallen -Overlord-");
    waifu.push("Narberal Gamma -Overlord-");
    waifu.push("Enri Emmot -Overlord-");
    waifu.push("Enju Aihara -Black Bullet-");
    waifu.push("Tina Sprout -Black Bullet-");
    waifu.push("Kisara Tendou -Black Bullet-");
    waifu.push("Midori Fuse -Black Bullet-");
    waifu.push("Kohina Hiruko -Black Bullet-");
    waifu.push("Kayo Senju -Black Bullet-");
    waifu.push("Sumire Muroto -Black Bullet-");
    waifu.push("Seitenshi -Black Bullet-");
    waifu.push("Asaka Mibu -Black Bullet-");
    waifu.push("Miori Shiba -Black Bullet-");
    waifu.push("Yuzuki Katagiri -Black Bullet-");
    waifu.push("Mashiro Shiina -The Pet Girl of Sakurasou-");
    waifu.push("Nanami Aoyama -The Pet Girl of Sakurasou-");
    waifu.push("Misaki Kamiigusa -The Pet Girl of Sakurasou-");
    waifu.push("Rita Ainsworth -The Pet Girl of Sakurasou-");
    waifu.push("Yuuko Kanda -The Pet Girl of Sakurasou-");
    waifu.push("Chihiro Sengoku -The Pet Girl of Sakurasou-");
    waifu.push("Dokuro Mitsukai -Bokusatsu Tenshi Dokuro-chan-");
    waifu.push("Zakuro Mitsukai -Bokusatsu Tenshi Dokuro-chan-");
    waifu.push("Sabato Mihashigo -Bokusatsu Tenshi Dokuro-chan-");
    waifu.push("Shizuki Minagami -Bokusatsu Tenshi Dokuro-chan-");
    waifu.push("Minami -Bokusatsu Tenshi Dokuro-chan-");
    waifu.push("Babel Mihashigo -Bokusatsu Tenshi Dokuro-chan-");
    waifu.push("Nagi Kirima -Boogiepop Phantom-");
    waifu.push("Boogiepop Phantom -Boogiepop Phantom-");
    waifu.push("Touka Miyashita -Boogiepop Phantom-");
    waifu.push("Kazuko Suema -Boogiepop Phantom-");
    waifu.push("Manaka Kisaragi -Boogiepop Phantom-");
    waifu.push("Kino -Kino no Tabi: The Beautiful World-");
    waifu.push("Sakura -Kino no Tabi: The Beautiful World-");
    waifu.push("Nimya Tchuhachkova -Kino no Tabi: The Beautiful World-");
    waifu.push("Shishou -Kino no Tabi: The Beautiful World-");
    waifu.push("Nike Remercier -The World is Still Beautiful-");
    waifu.push("Kara Remercier -The World is Still Beautiful-");
    waifu.push("Iraha Remercier -The World is Still Beautiful-");
    waifu.push("Mira Remercier -The World is Still Beautiful-");
    waifu.push("Nia Remercier -The World is Still Beautiful-");
    waifu.push("Tohara -The World is Still Beautiful-");
    waifu.push("Sheila -The World is Still Beautiful-");
    waifu.push("Mina -The World is Still Beautiful-");
    waifu.push("Liza -The World is Still Beautiful-");
    waifu.push("Amaluna Luirasael -The World is Still Beautiful-");
    waifu.push("Erio Touwa -Denpa Onna to Seishun Otoko-");
    waifu.push("Ryuuko Mifune -Denpa Onna to Seishun Otoko-");
    waifu.push("Maekawa -Denpa Onna to Seishun Otoko-");
    waifu.push("Meme Touwa -Denpa Onna to Seishun Otoko-");
    waifu.push("Yashiro Hoshimiya -Denpa Onna to Seishun Otoko-");
    waifu.push("Milinda Brantini -Heavy Object-");
    waifu.push("Frolaytia Capistrano -Heavy Object-");
    waifu.push("Ito Hikiotani -Punch Line-");
    waifu.push("Meika Daihatsu -Punch Line-");
    waifu.push("Rabura Chichibu -Punch Line-");
    waifu.push("Mikatan Narugino -Punch Line-");
    waifu.push("Himiko -Btooom!-");
    waifu.push("Hidemi Kinoshita -Btooom!-");
    waifu.push("Mahiru Inami -Working!!!-");
    waifu.push("Popura Taneshima -Working!!!-");
    waifu.push("Aoi Yamada -Working!!!-");
    waifu.push("Yachiyo Todoroki -Working!!!-");
    waifu.push("Kyouko Shirafuji -Working!!!-");
    waifu.push("Izumi Takanashi -Working!!!-");
    waifu.push("Maya Matsumoto -Working!!!-");
    waifu.push("Nazuna Takanashi -Working!!!-");
    waifu.push("Kozue Takanashi -Working!!!-");
    waifu.push("Kazue Takanashi -Working!!!-");
    waifu.push("Haruna Otoo -Working!!!-");
    waifu.push("Mitsuki Mashiba -Working!!!-");
    waifu.push("Alisa Illinichina Amiella -God Eater-");
    waifu.push("Rikka Kusunoki -God Eater-");
    waifu.push("Sakuya Tachibana -God Eater-");
    waifu.push("Tsubaki Amamiya -God Eater-");
    waifu.push("Shio -God Eater-");
    waifu.push("Hibari Takeda -God Eater-");
    waifu.push("Fremy Speeddraw -Rokka: Braves of the Six Flowers-");
    waifu.push("Nashetania Loei Piena Augustra -Rokka: Braves of the Six Flowers-");
    waifu.push("Mora Chester -Rokka: Braves of the Six Flowers-");
    waifu.push("Chamo Rosso -Rokka: Braves of the Six Flowers-");
    waifu.push("Shirayuki -Snow White with the Red Hair-");
    waifu.push("Kiki Seiran -Snow White with the Red Hair-");
    waifu.push("Garack Gazelt -Snow White with the Red Hair-");
    waifu.push("Nao Tomori  -Charlotte-");
    waifu.push("Ayumi Otosaka -Charlotte-");
    waifu.push("Yusa Kurobane -Charlotte-");
    waifu.push("Misa Kurobane -Charlotte-");
    waifu.push("Yumi Shirayanagi -Charlotte-");
    waifu.push("Megumi Tadokoro -Shokugeki no Souma-");
    waifu.push("Erina Nakiri -Shokugeki no Souma-");
    waifu.push("Alice Nakiri -Shokugeki no Souma-");
    waifu.push("Ikumi Mito -Shokugeki no Souma-");
    waifu.push("Hinako Inui -Shokugeki no Souma-");
    waifu.push("Hisako Arato -Shokugeki no Souma-");
    waifu.push("Yuuki Yoshino -Shokugeki no Souma-");
    waifu.push("Ryouko Sakaki -Shokugeki no Souma-");
    waifu.push("Nao Sadatsuka -Shokugeki no Souma-");
    waifu.push("Mayumi Kurase -Shokugeki no Souma-");
    waifu.push("Rory Mercury -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Tuka Luna Marceau -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Lelei la Lelena -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Piña Co Lada -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Shino Kurebayashi -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Mari Kurokawa -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Yao Haa Dushi -Gate Jieitai Kanochi nite, Kaku Tatakaeri-");
    waifu.push("Myucel Foaran -Outbreak Company-");
    waifu.push("Petralka Anne Eldante III -Outbreak Company-");
    waifu.push("Elbia Hanaiman -Outbreak Company-");
    waifu.push("Minori Koganuma -Outbreak Company-");
    waifu.push("Romilda Garde -Outbreak Company-");
    waifu.push("Luna  -Outbreak Company-");
    waifu.push("Momo Belia Deviluke -To LOVE-Ru-");
    waifu.push("Konjiki no Yami -To LOVE-Ru-");
    waifu.push("Mea Kurosaki -To LOVE-Ru-");
    waifu.push("Lala Satalin Deviluke -To LOVE-Ru-");
    waifu.push("Yui Kotegawa -To LOVE-Ru-");
    waifu.push("Mikan Yuuki -To LOVE-Ru-");
    waifu.push("Haruna Sairenji -To LOVE-Ru-");
    waifu.push("Nana Astar Deviluke -To LOVE-Ru-");
    waifu.push("Nemesis -To LOVE-Ru-");
    waifu.push("Tearju Lunatique -To LOVE-Ru-");
    waifu.push("Shizu Murasame -To LOVE-Ru-");
    waifu.push("Risa Momioka -To LOVE-Ru-");
    waifu.push("Aya Fujisaki -To LOVE-Ru-");
    waifu.push("Mio Sawada -To LOVE-Ru-");
    waifu.push("Rin Kujou -To LOVE-Ru-");
    waifu.push("Saki Tenjouin -To LOVE-Ru-");
    waifu.push("Ryouko Mikado -To LOVE-Ru-");
    waifu.push("Ui Wakana -My Wife is the Student Council President-");
    waifu.push("Rin Misumi -My Wife is the Student Council President-");
    waifu.push("Ayane Niikura -My Wife is the Student Council President-");
    waifu.push("Karen Fujisaki -My Wife is the Student Council President-");
    waifu.push("Mato Sawatari -My Wife is the Student Council President-");
    waifu.push("Sun Seto -Seto no Hanayome-");
    waifu.push("Lunar Edomae -Seto no Hanayome-");
    waifu.push("Akeno Shiranui -Seto no Hanayome-");
    waifu.push("Maki -Seto no Hanayome-");
    waifu.push("Mawari Zenigata -Seto no Hanayome-");
    waifu.push("Ren Seto -Seto no Hanayome-");
    waifu.push("Iinchou -Seto no Hanayome-");
    waifu.push("Mother Michishio -Seto no Hanayome-");
    waifu.push("Asako Nakamura -Ushio to Tora-");
    waifu.push("Mayuko Inoue -Ushio to Tora-");
    waifu.push("Rin Toosaka -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Illyasviel von Einzbern -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Chloe von Einzbern -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Miyu Edelfelt -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Luviagelita Edelfelt -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Irisviel von Einzbern -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Taiga Fujimura -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Caren Ortensia -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Bazett Fraga McRemitz -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Sella -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Leysritt -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Magical Ruby -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Tatsuko Gakumazawa -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Magical Sapphire -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Suzuka Kurihara -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Nanaki Moriyama -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Mimi Katsura -Fate/kaleid liner Prisma*Illya-");
    waifu.push("Rinko Yamato -Ore Monogatari!!-");
    waifu.push("Ai Sunakawa -Ore Monogatari!!-");
    waifu.push("Mariya Saijou -Ore Monogatari!!-");
    waifu.push("Michiru Matsushima -Grisaia no Kajitsu-");
    waifu.push("Amane Suou -Grisaia no Kajitsu-");
    waifu.push("Yumiko Sakaki -Grisaia no Kajitsu-");
    waifu.push("Sachi Komine -Grisaia no Kajitsu-");
    waifu.push("Makina Irisu -Grisaia no Kajitsu-");
    waifu.push("Kazuki Kazami -Grisaia no Kajitsu-");
    waifu.push("Asako Kusakabe -Grisaia no Kajitsu-");
    waifu.push("Yuria Harudera -Grisaia no Kajitsu-");
    waifu.push("Chizuru Tachibana -Grisaia no Kajitsu-");
    waifu.push("Milliela Stanfield -Grisaia no Kajitsu-");
    waifu.push("Chiara Farrell -Grisaia no Kajitsu-");
    waifu.push("Agnes Garrett -Grisaia no Kajitsu-");
    waifu.push("Inori Yuzuriha -Guilty Crown-");
    waifu.push("Ayase Shinomiya -Guilty Crown-");
    waifu.push("Tsugumi -Guilty Crown-");
    waifu.push("Hare Menjou -Guilty Crown-");
    waifu.push("Mana Ouma -Guilty Crown-");
    waifu.push("Haruka Ouma -Guilty Crown-");
    waifu.push("Naru Narusegawa -Love Hina-");
    waifu.push("Motoko Aoyama -Love Hina-");
    waifu.push("Shinobu Maehara -Love Hina-");
    waifu.push("Kaolla Su -Love Hina-");
    waifu.push("Mitsune Konno -Love Hina-");
    waifu.push("Mutsumi Otohime -Love Hina-");
    waifu.push("Haruka Urashima -Love Hina-");
    waifu.push("Amalla Su -Love Hina-");
    waifu.push("Mei Narusegawa -Love Hina-");
    waifu.push("Tsuruko Aoyama -Love Hina-");
    waifu.push("Sarah McDougal -Love Hina-");
    waifu.push("Miharu Sena Kanaka -Girls Bravo-");
    waifu.push("Kirie Kojima -Girls Bravo-");
    waifu.push("Risa Fukuyama -Girls Bravo-");
    waifu.push("Koyomi Hare Nanaka -Girls Bravo-");
    waifu.push("Tomoka Lana Jude -Girls Bravo-");
    waifu.push("Yukina -Girls Bravo-");
    waifu.push("Maharu Sena Kanaka -Girls Bravo-");
    waifu.push("Lilica Stacy -Girls Bravo-");
    waifu.push("Hijiri Kanata -Girls Bravo-");
    waifu.push("Louise Françoise Le Blanc de La Vallière -Zero no Tsukaima-");
    waifu.push("Charlotte Helene dOrleans -Zero no Tsukaima-");
    waifu.push("Henrietta de Tristain -Zero no Tsukaima-");
    waifu.push("Siesta -Zero no Tsukaima-");
    waifu.push("Kirche Augusta Frederica von Anhalt-Zerbst -Zero no Tsukaima-");
    waifu.push("Montmorency Margarita la Fere de Montmorency -Zero no Tsukaima-");
    waifu.push("Asuka Langley Soryu -Neon Genesis Evangelion-");
    waifu.push("Rei Ayanami -Neon Genesis Evangelion-");
    waifu.push("Misato Katsuragi -Neon Genesis Evangelion-");
    waifu.push("Ritsuko Akagi -Neon Genesis Evangelion-");
    waifu.push("Yui Ikari -Neon Genesis Evangelion-");
    waifu.push("Maya Ibuki -Neon Genesis Evangelion-");
    waifu.push("Hikari Horaki -Neon Genesis Evangelion-");
    waifu.push("Daedalus -Heavens Lost Property-");
    waifu.push("Ikaros -Heavens Lost Property-");
    waifu.push("Nymph -Heavens Lost Property-");
    waifu.push("Astraea -Heavens Lost Property-");
    waifu.push("Mikako Satsukitane -Heavens Lost Property-");
    waifu.push("Sohara Mitsuki -Heavens Lost Property-");
    waifu.push("Hiyori Kazane -Heavens Lost Property-");
    waifu.push("Chaos -Heavens Lost Property-");
    waifu.push("Iona -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Takao -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Kongou -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Haruna -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Hyuuga -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Kirishima -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Maya -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Iori Watanuki -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Makie Osakabe -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("I-400 -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("I-402 -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Shizuka Hozumi -Aoki Hagane no Arpeggio Ars Nova-");
    waifu.push("Chitoge Kirisaki -Nisekoi-");
    waifu.push("Kosaki Onodera -Nisekoi-");
    waifu.push("Marika Tachibana -Nisekoi-");
    waifu.push("Seishirou Tsugumi -Nisekoi-");
    waifu.push("Ruri Miyamoto -Nisekoi-");
    waifu.push("Nanako Onodera -Nisekoi-");
    waifu.push("Haru Onodera -Nisekoi-");
    waifu.push("Hana Kirisaki -Nisekoi-");
    waifu.push("Paula McCoy -Nisekoi-");
    waifu.push("Kyouko -Nisekoi-");
    waifu.push("Tooka Yatogami -Date A Live-");
    waifu.push("Kotori Itsuka -Date A Live-");
    waifu.push("Origami Tobiichi -Date A Live-");
    waifu.push("Kurumi Tokisaki -Date A Live-");
    waifu.push("Yoshino -Date A Live-");
    waifu.push("Reine Murasame -Date A Live-");
    waifu.push("Mana Takamiya -Date A Live-");
    waifu.push("Mii Fujibakama -Date A Live-");
    waifu.push("Hinako Shiizaki -Date A Live-");
    waifu.push("Tamae Okamine -Date A Live-");
    waifu.push("Miku Izayoi -Date A Live-");
    waifu.push("Yuzuru Yamai -Date A Live-");
    waifu.push("Kaguya Yamai -Date A Live-");
    waifu.push("Kaguya Yamai -Date A Live-");
    waifu.push("Tsukiumi -Sekirei-");
    waifu.push("Musubi -Sekirei-");
    waifu.push("Kazehana -Sekirei-");
    waifu.push("Kusano -Sekirei-");
    waifu.push("Matsu -Sekirei-");
    waifu.push("Homura -Sekirei-");
    waifu.push("Miya Asama -Sekirei-");
    waifu.push("Uzume -Sekirei-");
    waifu.push("Benitsubasa -Sekirei-");
    waifu.push("Karasuba -Sekirei-");
    waifu.push("Shiina -Sekirei-");
    waifu.push("Akitsu -Sekirei-");
    waifu.push("Hikari -Sekirei-");
    waifu.push("Hibiki -Sekirei-");
    waifu.push("Yukari Sahashi -Sekirei-");
    waifu.push("Takami Sahashi -Sekirei-");
    waifu.push("Shizuno Urushibara -Seiken Tsukai no World Break-");
    waifu.push("Satsuki Ranjou -Seiken Tsukai no World Break-");
    waifu.push("Elena Arshavina -Seiken Tsukai no World Break-");
    waifu.push("Maya Shimon -Seiken Tsukai no World Break-");
    waifu.push("Angela Johnson -Seiken Tsukai no World Break-");
    waifu.push("Tokiko Kanzaki -Seiken Tsukai no World Break-");
    waifu.push("Mari Shimon -Seiken Tsukai no World Break-");
    waifu.push("Haruka Momochi -Seiken Tsukai no World Break-");
    waifu.push("Sofia Mertesacker -Seiken Tsukai no World Break-");
    waifu.push("Vasilisa Yuryevna Mostovaya -Seiken Tsukai no World Break-");
    waifu.push('Kazane Kagari -Witch Craft Works-');
    waifu.push('Medusa -Witch Craft Works-');
    waifu.push('Kasumi Takamiya -Witch Craft Works-');
    waifu.push('Kasumi Takamiya -Witch Craft Works-');
    waifu.push('Chronoire Schwarz VI -Witch Craft Works-');
    waifu.push('Tanpopo Kuraishi  -Witch Craft Works-');
    waifu.push('Ayaka Kagari  -Witch Craft Works-');
    waifu.push('Mikoto Nishina -Phantom Breaker-');
    waifu.push('Waka Kumon -Phantom Breaker-');
    waifu.push('Itsuki Kouno -Phantom Breaker-');
    waifu.push('Yuzuha Fujibayashi -Phantom Breaker-');
    waifu.push('M -Phantom Breaker-');
    waifu.push('Cocoa -Phantom Breaker-');
    waifu.push('Ria Tojo -Phantom Breaker-');
    waifu.push('Fin -Phantom Breaker-');
    waifu.push('Sena Aoi -Chaos;Head-');
    waifu.push('Ayase Kishimoto -Chaos;Head-');
    waifu.push('Yua Kusunoki -Chaos;Head-');
    waifu.push('Nanami Nishijou -Chaos;Head-');
    waifu.push('Kozue Orihara -Chaos;Head-');
    waifu.push('Rimi Sakihata -Chaos;Head-');
    waifu.push('Junna Daitoku -Robotic;Notes-');
    waifu.push('Kona Furugoori -Robotic;Notes-');
    waifu.push('Akiho Senomiya -Robotic;Notes-');
    waifu.push('Airi Yukifune -Robotic;Notes-');
    waifu.push('Mizuka Irei -Robotic;Notes-');
    waifu.push('Misaki Senomiya -Robotic;Notes-');
    waifu.push('Angela Balzac -Expelled from Paradise-');
    waifu.push('Akiko Himenokouji -OniAi-');
    waifu.push('Anastasia Nasuhara -OniAi-');
    waifu.push('Arashi Nikaidou  -OniAi-');
    waifu.push('Ginbei Haruomi Sawatari -OniAi-');
    waifu.push('Mitsuki Kanzaki -Saikin Imouto no Yousu ga Chotto Okashiinda ga-');
    waifu.push('Hiyori Kotobuki -Saikin Imouto no Yousu ga Chotto Okashiinda ga-');
    waifu.push('Neko -Saikin Imouto no Yousu ga Chotto Okashiinda ga-');
    waifu.push('Umaru Doma -Himouto! Umaru-chan-');
    waifu.push('Nana Ebina -Himouto! Umaru-chan-');
    waifu.push('Kanou -Himouto! Umaru-chan-');
    waifu.push('Kirie Motoba -Himouto! Umaru-chan-');
    waifu.push('Sylphynford Tachibana -Himouto! Umaru-chan-');
    waifu.push('Reiko Arisugawa -Shomin Sample-');
    waifu.push('Karen Jinryou -Shomin Sample-');
    waifu.push('Hakua Shiodome -Shomin Sample-');
    waifu.push('Aika Tenkuubashi -Shomin Sample-');
    waifu.push('Eri Hanae -Shomin Sample-');
    waifu.push('Miyuki Kujou -Shomin Sample-');
    waifu.push('Fubuki -One Punch Man-');
    waifu.push('Tatsumaki -One Punch Man-');
    waifu.push('Kuon -Utawarerumono Itsuwari no Kamen-');
    waifu.push('Aruruu -Utawarerumono-');
    waifu.push('Atui -Utawarerumono Itsuwari no Kamen-');
    waifu.push('Karura -Utawarerumono-');
    waifu.push('Karura -Utawarerumono-');
    waifu.push('Touka -Utawarerumono-');
    waifu.push('Sarana -Utawarerumono Itsuwari no Kamen-');
    waifu.push('Rurutie -Utawarerumono Itsuwari no Kamen-');
    waifu.push('Uruuru -Utawarerumono Itsuwari no Kamen-');
    waifu.push('Kuya Amururineuruka -Utawarerumono-');
    waifu.push('Kamyu -Utawarerumono-');
    waifu.push('Mutsumi -Utawarerumono-');
    waifu.push('Urtori -Utawarerumono-');
    waifu.push('Yuzuha -Utawarerumono-');
    waifu.push('Super Sonico -Super Sonico the Animation-');
    waifu.push('Suzu Fujimi -Super Sonico the Animation-');
    waifu.push('Fuuri Watanuki -Super Sonico the Animation-');
    waifu.push('Ena Fujimi -Super Sonico the Animation-');
    waifu.push('Ein -Phantom Requiem for the Phantom-');
    waifu.push('Cal Devens -Phantom Requiem for the Phantom-');
    waifu.push('Claudia McCunnen -Phantom Requiem for the Phantom-');
    waifu.push('Elucia de Lute Ima -The World God Only Knows-');
    waifu.push('Haqua du Lot Herminium -The World God Only Knows-');
    waifu.push('Kusunoki Kasuga -The World God Only Knows-');
    waifu.push('Mari Katsuragi -The World God Only Knows-');
    waifu.push('Chihiro Kosaka -The World God Only Knows-');
    waifu.push('Jun Nagase -The World God Only Knows-');
    waifu.push('Yuri Nikaido -The World God Only Knows-');
    waifu.push('Yotsuba Sugimoto -The World God Only Knows-');
    waifu.push('Ayumi Takahara -The World God Only Knows-');
    waifu.push('Asami Yoshino -The World God Only Knows-');
    waifu.push('Mio Aoyama -The World God Only Knows-');
    waifu.push('Apollo -The World God Only Knows-');
    waifu.push('Tenri Ayukawa -The World God Only Knows-');
    waifu.push('Diana -The World God Only Knows-');
    waifu.push('Nora Floriann Leoria -The World God Only Knows-');
    waifu.push('Yui Goidou -The World God Only Knows-');
    waifu.push('Minami Ikoma -The World God Only Knows-');
    waifu.push('Tsukiyo Kujou -The World God Only Knows-');
    waifu.push('Lune -The World God Only Knows-');
    waifu.push('Mars -The World God Only Knows-');
    waifu.push('Kanon Nakagawa -The World God Only Knows-');
    waifu.push('Shiori Shiomiya -The World God Only Knows-');
    waifu.push('Vulcanus -The World God Only Knows-');
    waifu.push('Mikuru Asahina -The Melancholy of Haruhi Suzumiyas-');
    waifu.push('Yuki Nagato -The Melancholy of Haruhi Suzumiya-');
    waifu.push('Haruhi Suzumiya -The Melancholy of Haruhi Suzumiya-');
    waifu.push('Ryouko Asakura -The Melancholy of Haruhi Suzumiya-');
    waifu.push('Tsuruya -The Melancholy of Haruhi Suzumiya-');
    waifu.push('Meryl Stryfe -Trigun-');
    waifu.push('Milly Thompson -Trigun-');
    waifu.push('Kyou Goshouin -And you thought there is never a girl online?-');
    waifu.push('Ako Tamaki -And you thought there is never a girl online?-');

    $.consoleLn('Going to set all waifus in 15 seconds...');
    setTimeout(function () { //Added a 15 sec delay so it has time to push everything, and does not overload the bot on boot up.
        for (var i = 0; i < waifu.length; i++) {
            $.inidb.set('waifus', 'waifu_' + i, waifu[i]);
            $.consoleLn('waifu #' + i + ' has been set.');
        }
        $.inidb.set('settings', 'waifus_pushed', true);
        $.inidb.SaveAll(true);
    }, 15 * 1000);
}

$.registerChatCommand("./commands/waifuCommand.js", "waifu");
$.registerChatCommand("./commands/waifuCommand.js", "mywaifus");
$.registerChatCommand("./commands/waifuCommand.js", "unlockwaifu");
$.registerChatCommand("./commands/waifuCommand.js", "unpackwaifu");
$.registerChatCommand("./commands/waifuCommand.js", "forcewaifu");
$.registerChatCommand("./commands/waifuCommand.js", "sendwaifu");
$.registerChatCommand("./commands/waifuCommand.js", "splitwaifu");
$.registerChatCommand("./commands/waifuCommand.js", "setwaifu");
$.registerChatCommand("./commands/waifuCommand.js", "delwaifu", 1);
$.registerChatCommand("./commands/waifuCommand.js", "addwaifu", 1);
$.registerChatCommand("./commands/waifuCommand.js", "editwaifu", 1);
