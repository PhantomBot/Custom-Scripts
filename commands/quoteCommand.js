$.getDate = function() {
	var datefmt = new java.text.SimpleDateFormat("MM-dd-yyyy");
	var cal = java.util.Calendar.getInstance(java.util.TimeZone.getTimeZone($.timeZone));
	var time = cal.getTime();
    var timestamp = datefmt.format(time);

    return timestamp;
}

if ($.inidb.get("quotes", "num_quotes") == null) {
	$.inidb.set("quotes", "num_quotes", 0);
}

$.on('command', function(event) {
    var sender = event.getSender();
    var command = event.getCommand();
    var argsString = event.getArguments().trim();
    var args = event.getArgs();

    if (command.equalsIgnoreCase("quote")) {
    	if (args.length == 0) {
    		if ($.inidb.get("quotes", "num_quotes") > 0) {
    	    var ran = $.randRange(0, parseInt($.inidb.get("quotes", "num_quotes")));
    		$.say("Quote #" + ran + " " + $.inidb.get("quotes", "quote_" + ran));
    		return;
    	} else {
    		$.say($.getWhisperString(sender) + "There are no quotes at this time, or the specified quote number does not exist.");
    		return;
    	}
    } else if (args.length == 1) {
    	if ($.inidb.get("quotes", "quote_" + parseInt(args[0]))) {
    	    $.say("Quote #" + parseInt(args[0]) + " " + $.inidb.get("quotes", "quote_" + parseInt(args[0])));
    	    return;
    	} else {
    	    $.say($.getWhisperString(sender) + "There are no quotes at this time, or the specified quote number does not exist.");
    	    return;
    	    }
    	}
    
    	var quotes = $.inidb.get("quotes", "num_quotes");
    	var message = "\"" + argsString.substring(argsString.indexOf(" ") + 1, argsString.length()) + "\"";
    	var messageEdit = "\"" + argsString.substring(argsString.indexOf(" ") + 2, argsString.length()) + "\"";
    	var game = $.getGame($.channelName);
    	var date = $.getDate();

    	if (args[0].equalsIgnoreCase("add")) {
    		if (!$.isModv3(sender, event.getTags())) {
                $.say($.getWhisperString(sender) + $.getWhisperString(sender) + $.modmsg);
                return;
            }
            if (args.length < 2) {
            	$.say($.getWhisperString(sender) + "Usage: !quote add (quote)");
            	return;
            }
            $.inidb.incr("quotes", "num_quotes", 1);
            $.inidb.set("quotes", "quote_" + quotes, message + "  [" + date + "] - " + game);
            $.say($.getWhisperString(sender) + "Quote " + message + " has been added!");
    	} 

    	if (args[0].equalsIgnoreCase("remove")) {
    		if (!$.isModv3(sender, event.getTags())) {
                $.say($.getWhisperString(sender) + $.getWhisperString(sender) + $.modmsg);
                return;
            }
            if (args.length < 2) {
            	$.say($.getWhisperString(sender) + "Usage: !quote remove (ID)");
            	return;
            } else if ($.inidb.get("quotes", "quote_" + parseInt(args[1])) == null) {
            	$.say($.getWhisperString(sender) + "That quote does not exist.");
            	return;
            }
            $.inidb.decr("quotes", "num_quotes", 1);
            $.inidb.del("quotes", "quote_" + parseInt(args[1]));
            $.say($.getWhisperString(sender) + "Quote #" + parseInt(args[1]) + " has been removed!");
    	} 

    	if (args[0].equalsIgnoreCase("edit")) {
    		if (!$.isModv3(sender, event.getTags())) {
                $.say($.getWhisperString(sender) + $.getWhisperString(sender) + $.modmsg);
                return;
            }
            if (args.length < 3) {
            	$.say($.getWhisperString(sender) + "Usage: !quote edit (ID) (quote)");
            	return;
            } else if ($.inidb.get("quotes", "quote_" + parseInt(args[1])) == null) {
            	$.say($.getWhisperString(sender) + "That quote does not exist.");
            	return;
            }
            $.inidb.set("quotes", "quote_" + parseInt(args[1]), messageEdit + "  [" + date + "] - " + game);
            $.say($.getWhisperString(sender) + "Quote #" + parseInt(args[1]) + " has been modified!");
    	}
    }
});

setTimeout(function(){ 
    if ($.moduleEnabled('./commands/quoteCommand.js')) {
        $.registerChatCommand("./commands/quoteCommand.js", "quote");
    }
}, 10 * 1000);
