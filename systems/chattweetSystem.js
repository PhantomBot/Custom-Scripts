(function () {
    var chatTweetToggle = ($.inidb.exists('chatTweet', 'chatTweetToggle') ? $.inidb.get('chatTweet', 'chatTweetToggle') : false),
        twitterAcount = ($.inidb.exists('chatTweet', 'twitterAcount') ? $.inidb.get('chatTweet', 'twitterAcount') : null),
        lastTweet = ($.inidb.exists('chatTweet', 'lastTweet') ? $.inidb.get('chatTweet', 'lastTweet') : null);

    function getTweet (url) { //Made by simeonF
    var HttpResponse = Packages.com.gmt2001.HttpResponse,
        HttpRequest = Packages.com.gmt2001.HttpRequest,
        HashMap = Packages.java.util.HashMap,
        response = HttpRequest.getData(HttpRequest.RequestType.GET, url, "", new HashMap());
        return response.content;
    };

    $.bind('command', function (event) {
        var sender = event.getSender(),
            command = event.getCommand(),
            argsString = event.getArguments().trim(),
            args = event.getArgs(),
            action = args[0],
            subAction = args[1];

        if (command.equalsIgnoreCase('chattweettoggle')) {
            if (!$.isAdmin(sender)) {
                $.say($.whisperPrefix(sender) + $.adminMsg);
                return;
            } 
            if (chatTweetToggle) {
                chatTweetToggle = false;
                $.inidb.set('chatTweet', 'chatTweetToggle', chatTweetToggle);
                $.say($.whisperPrefix(sender) + 'Chat twete disabled.');
                return;
            } else {
                chatTweetToggle = true;
                $.inidb.set('chatTweet', 'chatTweetToggle', chatTweetToggle);
                $.say($.whisperPrefix(sender) + 'Chat twete enabled.');
                return;
            }
        }

        if (command.equalsIgnoreCase('settwitter')) {
            if (!$.isAdmin(sender)) {
                $.say($.whisperPrefix(sender) + $.adminMsg);
                return;
            } else if (!action) {
                $.say($.whisperPrefix(sender) + 'Usage: !settwitter (twitter)');
                return;
            }
            twitterAcount = argsString;
            $.inidb.set('chatTweet', 'twitterAcount', twitterAcount);
            $.say($.whisperPrefix(sender) + 'Twitter set to ' + twitterAcount);
            return;
        }
    
        if (command.equalsIgnoreCase('lasttweet')) {
        	$.say('Last tweet from @' + twitterAcount + ' - ' + lastTweet);
        	return;
        }
    });

    $.bind('initReady', function () {
        if ($.bot.isModuleEnabled('./systems/chattweetSystem.js')) {
            $.registerChatCommand('./systems/chattweetSystem.js', 'chattweettoggle', 1);
            $.registerChatCommand('./systems/chattweetSystem.js', 'settwitter', 1);
            $.registerChatCommand('./systems/chattweetSystem.js', 'lasttweet', 1);
            setInterval(function () {
                if (twitterAcount != null) {
                    if (chatTweetToggle) {
                        $.say('Live twitter feed from @' + twitterAcount + ' - ' + getTweet(twitterAcount));
                        return;
                    }
                }
            }, 75000);
        }
    });
})();
