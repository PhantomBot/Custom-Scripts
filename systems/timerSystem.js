$.Timer = parseInt($.inidb.get("timers", "interval"));
$.Timers = $.inidb.get("timers", "timer");

if ($.Timer == undefined || $.Timer == null || isNaN($.Timer)) {
    $.Timer = $.inidb.set("timers", "interval", "5");
}

if ($.Timers == undefined || $.Timers == null || isNaN($.Timers)) {
    $.Timers = $.inidb.set("timers", "timer", "false");
}

$.on('command', function (event) {
    var sender = event.getSender();
    var command = event.getCommand();
    var argsString = event.getArguments().trim();
    var args = event.getArgs();
    var cmd = argsString.substring(argsString.indexOf(" ") + 1, argsString.length());

    if (command.equalsIgnoreCase("timer")) {
    	if (!$.isAdmin(sender)) {
    		$.say($.getWhisperString(sender) + $.adminmsg);
    		return;
    	} else if (args.length == 0) {
    		$.say($.getWhisperString(sender) + "Usage: !timer [add / remove / get]");
    		return;
    	} else if (args[0].equalsIgnoreCase("add")) {
    		if (args.length < 2) {
    			$.say($.getWhisperString(sender) + "Usage: !timer add (cmd)");
    		    return;
    		}
            var num = $.inidb.get("timers", "num_timers");
            if (num == null || isNaN(num)) {
                num = 0;
            }
    		$.inidb.incr("timers", "num_timers", 1);
    		$.inidb.set("timers", "timer_" + num, cmd);
    		$.say($.getWhisperString(sender) + "Timer added to the list.");
    		return;
    	} else if (args[0].equalsIgnoreCase("remove")) {
    		if (args.length < 2) {
    			$.say($.getWhisperString(sender) + "Usage: !timer remove (id)");
    		    return;
    		} else if ($.inidb.get("timers", "timer_" + parseInt(args[1])) == null) {
    			$.say($.getWhisperString(sender) + "That Timer does not exist.");
    			return;
    		}
    		$.inidb.decr("timers", "num_timers", 1);
    		$.inidb.del("timers", "timer_" + parseInt(args[1]));
    		$.say($.getWhisperString(sender) + "Timer removed from the list.");
    		return;
    	} else if (args[0].equalsIgnoreCase("get")) {
    		if (args.length < 2) {
    			$.say($.getWhisperString(sender) + "Usage: !timer get (id) - id's go from 1 to " + parseInt($.inidb.get("timers", "num_timers")));
    		    return;
    		} else if ($.inidb.get("timers", "timer_" + parseInt(args[1])) == null) {
    			$.say($.getWhisperString(sender) + "That Timer does not exist.");
    			return;
    		}
    		$.say($.getWhisperString(sender) + "Timer #" + parseInt(args[1]) + " -> \"" + $.inidb.get("timers", "timer_" + parseInt(args[1])) + "\"");
    		return;
    	} else if (args[0].equalsIgnoreCase("interval")) {
            if (args.length < 2) {
                $.say($.getWhisperString(sender) + "Usage: !timer interval (time)");
                return;
            } 

            if (args[1] > 5) {
                $.say("interval needs to be more then 5");
                return;
            }
            $.inidb.set("timers", "interval", parseInt(args[1]));
            $.say("Timer interval set to " + args[1] + " minutes.");
            return;
        } else if (args[0].equalsIgnoreCase("toggle")) {
            if ($.Timers == "false") {
                $.Timers = "true";
                $.inidb.set("timers", "timer", "true");
                $.say("timers enabled")
            } else {
                $.Timers = "false";
                $.inidb.set("timers", "timer", "false");
                $.say("timers disabled.")
            }
        }
    }
});

$.trigger = 0;
$.timer.addTimer("./systems/timerSystem.js", "timers", true, function() {
    if ($.Timers == "true") {
       var EventBus = Packages.me.mast3rplan.phantombot.event.EventBus;
       var CommandEvent = Packages.me.mast3rplan.phantombot.event.command.CommandEvent;
       var cmd = $.inidb.get("timers", "timer_" + $.trigger);
       var prm = "";

       $.trigger++;

       if ($.trigger >= parseInt($.inidb.get("timers", "num_timers"))) {
            $.trigger = 0;
       }
       
       EventBus.instance().post(new CommandEvent($.botname, cmd, prm));
   }
}, $.Timer * 60 * 1000);
