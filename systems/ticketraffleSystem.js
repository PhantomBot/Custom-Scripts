(function () {
	var cost = 0,
	    entries = [],
	    maxEntries = 0,
	    followers = false,
	    raffleStatus = false,
        a = '';

    function checkArgs (user, maxEntries, cost, followers) {
    	if (raffleStatus) {
    		$.say($.whisperPrefix(user) + 'A ticket raffle is already opened.');
    		return;
    	}

    	if (!max) {
    		$.say($.whisperPrefix(sender) + 'Usage: !traffle open (max entries) (cost) (-followers)');
    		return;
    	}

    	if (max) {
    		maxEntries = parseInt(maxEntries);
    	}

        if (cost) {
            cost = parseInt(cost);
        }

    	if (follower && follower.equalsIgnoreCase('-followers')) {
    		followers = true;
            a = 'You need to be following to enter.';
    	}
    	openRaffle(maxEntries, followers, cost);
    };

    function openRaffle (maxEntries, followers, cost) {
    	$.say('Ticket raffle is now open! You are only allowed to enter ' + maxEntries + ' times. Tickets cost ' + cost + ' ' + $.pointname + '. ' + a);
    	raffleStatus = true;
    };

    function closeRaffle (user) {
    	if (!raffleStatus) {
    		$.say($.whisperPrefix(user) + 'There is no ticket raffle opened.');
    		return;
    	}

    	$.say('The ticket raffle is now closed.');
    	winner();
    	raffleStatus = false;
    };

    function winner (force) {
    	if (entries.length == 0) {
    		$.say('The ticket raffle ended. No one entered.');
    		return;
    	}

    	if (force) {
            $.say('Choosing a new winner...');
    		$.say('New winner is: ' + $.username.resolve($.randElement(entries)) + '!');
    		return;
    	}
    	winner = $.randElement(entries);
    	$.say('The Winner of this ticket raffle is: ' + $.username.resolve(winner) + '!');
    };

    function enterRaffle (user, force, times) {
    	if (force) {
    		entries.push(user);
    		return;
    	}

        if (!raffleStatus) {
            $.say($.whisperPrefix(user) + 'There is no ticket raffle opened.');
            return;
        }

    	if ($.list.contains(currentRaffle.users, user).length > maxEntries) {
    		$.say($.whisperPrefix(user) + 'You hit the maximum amount of times that you can enter.');
    		return;
    	}

    	if (followers) {
    		if (!$.user.isFollower(user)) {
    			$.say($.whisperPrefix(user) + 'You need to be following to enter.');
    			return;
    		}
    	}

    	if (cost > 0) {
    		if (cost > $.getUserPoints(user)) {
    			$.say($.whisperPrefix(user) + 'You don\'t have enough points to enter.');
    			return;
    		}
    	}

    	for (var i = 0; i < times; i++) {
    	    entries.push(user);
    	}
    };

    $.bind('command', function (event) {
    	var sender = event.getSender(),
    	    command = event.getCommand(),
    	    argString = event.getArguments(),
    	    args = event.getArgs(),
    	    action = args[0];

    	if (command.equalsIgnoreCase('traffle')) {
    		if (!$.isModv3(sender, event.getTags())) {
    			$.say($.whisperPrefix(sender) + $.modMsg);
    			return;
            }

    		if (!action) {
    			$.say($.whisperPrefix(sender) + 'Usage: !traffle open (max entries) (cost) (-followers)');
    			return;
            }
    	
    		if (action.equalsIgnoreCase('open')) {
    			checkArgs(sender, args[1], args[2], args[3]);
            }
    	
    		if (action.equalsIgnoreCase('close')) {
    			closeRaffle(sender);
    		}

            if (action.equalsIgnoreCase('repick')) {
                winner(true);
            }
    	}
    });

    $.bind('initReady', function () {
    	if ($.bot.isModuleEnabled('./systems/ticketRaffleSystem.js')) {
    		$.registerChatCommand('./systems/ticketRaffleSystem.js', 'traffle', 2);
    	}
    });

    $.tRaffleStatus = raffleStatus;
    $.tRaffleEnter = enterRaffle;
})();
