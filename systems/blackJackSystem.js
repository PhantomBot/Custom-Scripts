Card = function(value, suit) {
    this.value = value;
    this.suit = suit;
}

Card.prototype.getValue = function() {
    return (this.value > 10) ? 10 : this.value;
};

Player = function(name) {
    this.name = name;
    this.hand = [];
    this.purse = 100;
    this.stake = 0;
}

Player.prototype.getHandTotal = function() {
    var handTotal = 0;
    for (var i = 0; i < this.hand.length; i++) {
        handTotal += (this.hand[i].value > 10) ? 10 : this.hand[i].value;
    }
    return handTotal;
};

Player.prototype.isBust = function() {
    return (this.getHandTotal() > 21);
};

Player.prototype.setStake = function(stake) {
    stake = parseInt(stake);
    if (isNaN(stake)) {
        $.say('Bad amount');
    }
    if (stake > this.purse) {
        $.say('Can’t stake more than purse');
    }
    this.stake = stake;
    this.decrementPurse(stake);
};

Player.prototype.decrementPurse = function(amount) {
    amount = parseInt(amount);
    if (isNaN(amount)) {
        $.say('Bad amount');
    }
    this.purse -= amount;
}

Player.prototype.incrementPurse = function(amount) {
    amount = parseInt(amount);
    if (isNaN(amount)) {
        $.say('Bad amount');
    }
    this.purse += amount;
}

var deck = [];

for (var i = 1; i < 14; i++) {
    deck.push(new Card(i, 'Club'));
    deck.push(new Card(i, 'Diamond'));
    deck.push(new Card(i, 'Heart'));
    deck.push(new Card(i, 'Spade'));
}

deck.sort(function() { 
    return (0.5 - Math.random());

});

var dealer =  new Player($.botName);
        
var players = [];

function endGame() {
    $.say($.botName + '\'s hand total at end of game: ' + dealer.getHandTotal());
    
    for (var key in players) {
                if (players[key].isBust()) {
                    $.say(players[key].name + ' bust (' + players[key].getHandTotal() + ')');
                }
                else {
                    if (dealer.isBust() || players[key].getHandTotal() > dealer.getHandTotal()) {
                        $.say(players[key].name + ' wins');
                        players[key].incrementPurse(players[key].stake * 2);
                        $.inidb.incr('points', winner, purse);
                    }
                    else {
                        $.say(players[key].name + ' loses');
                        var loser = $.username.resolve(players[key].name);
                    }
                }
                
                players[key].setStake(0);
                
                $.say(players[key].name + 'has a new purse of ' + players[key].purse);
                var winner = $.username.resolve(players[key]);
            }
            $.say('Game over');
            var players = [];
            var dealer = new Player($.botName);

}


function dealersTurn() {
    $.say($.botName + ' gets a turn');
    $.say($.botName + ' has a hand total: ' + dealer.getHandTotal());
    while (dealer.getHandTotal() < 18) {
        dealer.hand.push(deck.pop());
        $.say($.botName + '\'s new hand total: ' + dealer.getHandTotal());
    }
    endGame();
}

$.on('command', function(event) {
var sender = event.getSender();
var username = $.username.resolve(sender);
var command = event.getCommand();
var argsString = event.getArguments().trim();
var args = event.getArgs();
	
	if (argsString.isEmpty()) {
        args = [];
    } else {
        args = argsString.split(" ");
    }

	if (command.equalsIgnoreCase("blackjack")) {
        players[username] = new Player(username);

        if (args[0] == null || isNaN(parseInt(args[0]))) {
            $.say($.getWhisperString(sender) + 'you need to place a stake, no playing for free!');
            return;
        } else {
            var stake = args[0];
            players[username].setStake(stake);
            $.say(username + ' has staked ' + stake);
            $.inidb.decr('points', username, stake);
        }

        dealer.hand.push(deck.pop());
        
        for (key in players) {
            players[key].hand.push(deck.pop());
        }

        $.say($.botName + ' hand total: ' + dealer.getHandTotal());

        dealer.hand.push(deck.pop());

        for (key in players) {
            players[key].hand.push(deck.pop());
        }

        $.say(username + ' hand total: ' + players[username].getHandTotal());

    }

	if (command.equalsIgnoreCase("hit")) {
        players[username].hand.push(deck.pop());
        $.say('Player hit');
        $.say('Player’s new hand total: ' + players[username].getHandTotal());
    	if (players[username].getHandTotal() > 20) {
        	if (players[username].getHandTotal() > 21) {
            	$.say('Player bust');
        	}
        	else {
            	$.say('Player has 21; moving to next player');
        	}	
        	$.dealersTurn();
    	}
    }

    if (command.equalsIgnoreCase("stay")) {
    $.say(username + ' has chosen to stay!');
    $.dealersTurn();
    }   
});
setTimeout(function(){ 
    if ($.moduleEnabled('./systems/blackJackSystem.js')) {
$.registerChatCommand("./systems/blackJackSystem.js", "blackjack");
$.registerChatCommand("./systems/blackJackSystem.js", "hit");
$.registerChatCommand("./systems/blackJackSystem.js", "stay");
}
},10 * 1000);
    
