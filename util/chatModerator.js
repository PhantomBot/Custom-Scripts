$.ChatModerator = {
    permitList: new Array(0),
    timedOutUserList: new Array(1),

    Links: $.inidb.get('settings', 'linksallowed').equalsIgnoreCase('1'),
    caps: $.inidb.get('settings', 'capsallowed').equalsIgnoreCase('1'),
    symbols: $.inidb.get('settings', 'symbolsallowed').equalsIgnoreCase('1'),
    repeat: $.inidb.get('settings', 'repeatallowed').equalsIgnoreCase('1'),
    grapheme: $.inidb.get('settings', 'graphemeallowed').equalsIgnoreCase('1'),
    ips: $.inidb.get('settings', 'ipsallowed').equalsIgnoreCase('1'),
    youtubelinks: $.inidb.get('settings', 'youtubeallowed').equalsIgnoreCase('1'),
    regulars: $.inidb.get('settings', 'regsallowed').equalsIgnoreCase('1'),
    subscribers: $.inidb.get('settings', 'subsallowed').equalsIgnoreCase('1'),
    
    Linksmessage: ($.inidb.get('settings', 'linksmessage') ? $.inidb.get('settings', 'linksmessage') : 'Don\'t post links without permission'),
    capsmessage: ($.inidb.get('settings', 'capsmessage') ? $.inidb.get('settings', 'capsmessage') : 'Don\'t overuse caps'),
    symbolsmessage: ($.inidb.get('settings', 'symbolsmessage') ? $.inidb.get('settings', 'symbolsmessage') : 'Don\'t overuse symbols'),
    repeatmessage: ($.inidb.get('settings', 'repeatmessage') ? $.inidb.get('settings', 'repeatmessage') : 'Don\'t spam repeating characters'),
    graphememessage: ($.inidb.get('settings', 'graphememessage') ? $.inidb.get('settings', 'graphememessage') : 'Don\'t overuse graphemes'),
    ipmessage: ($.inidb.get('settings', 'ipmessage') ? $.inidb.get('settings', 'ipmessage') : 'Don\'t post ip addresses'),
    blacklistMessage: ($.inidb.get('settings', 'blacklistmessage') ? $.inidb.get('settings', 'blacklistmessage') : 'Don\'t use blacklisted phrases'),

    capslimit: (parseInt($.inidb.get('settings', 'capstriggerlength')) ? parseInt($.inidb.get('settings', 'capstriggerlength')) : 70),
    symbolslimit: (parseInt($.inidb.get('settings', 'symbolsrepeatlimit')) ? parseInt($.inidb.get('settings', 'symbolsrepeatlimit')) : 24),
    repeatlimit: (parseInt($.inidb.get('settings', 'repeatlimit')) ? parseInt($.inidb.get('settings', 'repeatlimit')) : 18),
    graphemelimit: (parseInt($.inidb.get('settings', 'graphemelimit')) ? parseInt($.inidb.get('settings', 'graphemelimit')) : 6),
    permittime: (parseInt($.inidb.get('settings', 'permittime')) ? parseInt($.inidb.get('settings', 'permittime')) : 120),
    capsratio: (parseInt($.inidb.get('settings', 'capstriggerratio')) ? parseInt($.inidb.get('settings', 'capstriggerratio')) : 0.45),

    blacklist: $.inidb.GetKeyList('blacklist', ''),
    whitelist: $.inidb.GetKeyList('whitelist', ''),
    getTotalBlacklist: (parseInt($.inidb.GetKeyList('blacklist', '').length) ? parseInt($.inidb.GetKeyList('blacklist', '').length) : 0),
    getTotalWhitelist: (parseInt($.inidb.GetKeyList('whitelist', '').length) ? parseInt($.inidb.GetKeyList('whitelist', '').length) : 0),

    timeoutmode: '',
};

$.ChatModerator.timeoutUserFor = function (sender, seconds) {
    $.say('.timeout ' + sender + ' ' + seconds);
    $.say('.timeout ' + sender + ' ' + seconds);
    setTimeout(function () {
        $.say('.timeout ' + sender + ' ' + seconds);
    }, 4000);
};

$.ChatModerator.setTimeoutUser = function (sender) {
    var t = setTimeout(function () {
        for (var i in $.ChatModerator.timedOutUserList) {
            if ($.ChatModerator.timedOutUserList[i].equalsIgnoreCase(sender)) {
                $.ChatModerator.timedOutUserList.splice(i, 1);
                break;
            }
        }
        clearTimeout(t);
    }, 60 * 60 * 1000);
};

$.ChatModerator.timeoutUser = function (sender) {
    for (var i in $.ChatModerator.timedOutUserList) {
        if ($.ChatModerator.timedOutUserList[i].equalsIgnoreCase(sender)) {
            $.ChatModerator.timeoutUserFor(sender, 600);
            $.ChatModerator.setTimeoutUser(sender);
            $.ChatModerator.timedOutUserList.push(sender);
            $.ChatModerator.timeoutwarningmsg = '(timeout)';
            return;
        }
    }
    $.ChatModerator.timeoutUserFor(sender, 5);
    $.ChatModerator.setTimeoutUser(sender);
    $.ChatModerator.timedOutUserList.push(sender);
    $.ChatModerator.timeoutwarningmsg = '(warning)';
    return;
};

$.ChatModerator.permitUser = function (user) {
  var t;
  $.ChatModerator.permitList.push(user);
  t = setTimeout(function () {
    var i;
    for (i in $.ChatModerator.permitList) {
        if ($.ChatModerator.permitList[i].equalsIgnoreCase(user)) {
            $.ChatModerator.permitList.splice(i, 1);
            break;
        }
    }
    clearTimeout(t);
    }, $.ChatModerator.permittime * 1000);
};

$.ChatModerator.ban = function (sender) {
    $.say('.timeout ' + sender);
    $.say('.timeout ' + sender);
    setTimeout(function () {
        $.say('.ban ' + sender);
    }, 2500);
};

$.ChatModerator.unBan = function (sender) {
    $.say('.unban ' + sender);
};

$.ChatModerator.clearChat = function () {
    $.say('.clear');
};

$.on('command', function (event) {
    var sender = event.getSender();
    var command = event.getCommand();
    var argsString = event.getArguments().trim();
    var argsString2 = argsString.substring(argsString.indexOf(' ') + 1, argsString.length());
    var args = event.getArgs();
    var subCommand = args[0];

    if (command.equalsIgnoreCase('purge')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !purge (user)');
            return;
        }
        $.ChatModerator.timeoutUserFor(args[0], 1);
        $.say(args[0] + ' has been purged.');
        $.logEvent('chatModerator.js', 647, sender + ' purged ' + args[0] + '.');
        return;
    } else if (command.equalsIgnoreCase('timeout')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !timeout (user)');
            return;
        }
        $.ChatModerator.timeoutUserFor(args[0], 600);
        $.say(args[0] + ' has been timed out.');
        $.logEvent('chatModerator.js', 647, sender + ' timed out ' + args[0] + '.');
        return;
    } else if (command.equalsIgnoreCase('ban')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !ban (user)');
            return;
        }
        $.ChatModerator.ban(args[0]);
        $.say(args[0] + ' has been banned.');
        $.logEvent('chatModerator.js', 647, sender + ' banned ' + args[0] + '.');
        return;
    } else if (command.equalsIgnoreCase('unban')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !unban (user)');
            return;
        }
        $.ChatModerator.unBan(args[0]);
        $.say(args[0] + ' has been un-banned.');
        $.logEvent('chatModerator.js', 647, sender + ' unbanned ' + args[0] + '.');
        return;
    } else if (command.equalsIgnoreCase('clear')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        }
        $.ChatModerator.clearChat();
        $.say(sender + ' cleared chat.');
        $.logEvent('chatModerator.js', 647, sender + ' cleared chat.');
    } else if (command.equalsIgnoreCase('permit')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !permit (user)');
            return;
        }
        $.ChatModerator.permitUser(args[0].toLowerCase());
        $.say($.username.resolve(args[0]) + ' is now permited to post one link for the next ' + $.ChatModerator.permittime + ' seconds.');
        $.logEvent('chatModerator.js', 647, sender + ' permited ' + args[0] + '.');
    } else if (command.equalsIgnoreCase('blacklistadd')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !blacklistadd (word/phrase/pattern)');
            return;
        }
        $.inidb.set('blacklist', 'phrase_' + $.ChatModerator.getTotalBlacklist, argsString);
        $.ChatModerator.getTotalBlacklist++;
        $.ChatModerator.blacklist = $.inidb.GetKeyList('blacklist', '');
        $.say($.getWhisperString(sender) + 'Phrase added to the blacklist.');
        $.logEvent('chatModerator.js', 647, sender + ' added blacklist phrase: ' + argsString + '.');
    } else if (command.equalsIgnoreCase('blacklistget')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !blacklistadd (ID)');
            return;
        }
        $.say($.inidb.get('blacklist', 'phrase_' + args[0]));
    } else if (command.equalsIgnoreCase('blacklistdel')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !blacklistdel (ID)');
            return;
        }
        $.inidb.del('blacklist', 'phrase_' + args[0]);
        $.ChatModerator.getTotalBlacklist--;
        $.ChatModerator.blacklist = $.inidb.GetKeyList('blacklist', '');
        $.say($.getWhisperString(sender) + 'Phrase removed from the blacklist.');
        $.logEvent('chatModerator.js', 647, sender + ' removed blacklist phrase: ' + argsString + '.');
    } else if (command.equalsIgnoreCase('whitelistadd')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !whitelistadd (word/phrase/pattern)');
            return;
        }
        $.inidb.set('whitelist', 'link_' + $.ChatModerator.getTotalWhitelist, argsString);
        $.ChatModerator.getTotalWhitelist++;
        $.ChatModerator.whitelist = $.inidb.GetKeyList('whitelist', '');
        $.say($.getWhisperString(sender) + 'URL added to the whitelist.');
        $.logEvent('chatModerator.js', 647, sender + ' whitelisted link: ' + argsString + '.');
    } else if (command.equalsIgnoreCase('whitelistget')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !whitelistget (ID)');
            return;
        }
        $.say($.inidb.get('whitelist', 'link_' + args[0]));
    } else if (command.equalsIgnoreCase('whitelistdel')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length == 0) {
            $.say($.getWhisperString(sender) + 'Usage: !whitelistdel (ID)');
            return;
        }
        $.inidb.del('whitelist', 'link_' + args[0]);
        $.ChatModerator.getTotalWhitelist--;
        $.ChatModerator.whitelist = $.inidb.GetKeyList('whitelist', '');
        $.say($.getWhisperString(sender) + 'URL removed from the whitelist.');
        $.logEvent('chatModerator.js', 647, sender + ' un-whitelisted link: ' + argsString + '.');
    } else if (command.equalsIgnoreCase('chatmod') || command.equalsIgnoreCase('moderation')) {
        if (!$.isModv3(sender, event.getTags())) {
            $.say($.getWhisperString(sender) + $.modmsg);
            return;
        } else if (args.length < 1) {
            $.say($.getWhisperString(sender) + '-> Usage: !chatmod (option) (new value)');
            $.say($.getWhisperString(sender) + '-> Options: linksallowed, capsallowed, symbolsallowed, repeatallowed, ipsallowed, graphemeallowed, regsallowed, youtubesallowed, subsaallowed');
            $.say($.getWhisperString(sender) + '-> More Options: linksmessage, capsmessage, symbolsmessage, repeatmessage, ipmessage, graphememessage, blacklistmessage, permitmessage');
            $.say($.getWhisperString(sender) + '-> More Options: permittime, capstriggerratio, capstriggerlength, symbolslimit, symbolsrepeatlimit, repeatlimit, graphemelimit');
            return;
        } else if (subCommand.equalsIgnoreCase('blacklistmessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current blacklist message is ' + $.ChatModerator.blacklistMessage + '. To change it use: !chatmod blacklistmessage <any text>');
            } else {
                $.inidb.set('settings', 'blacklistmessage', val);
                $.say($.getWhisperString(sender) + 'Changed blacklist message to: ' + argsString2 + '!');
            }
        } else if (subCommand.equalsIgnoreCase('capsallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.caps) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say($.getWhisperString(sender) + 'Caps are currently ' + val + '. To change it use: !chatmod capsallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'capsallowed', val);
                $.ChatModerator.caps = val;
                if ($.ChatModerator.caps) {
                    $.say($.getWhisperString(sender) + 'Caps are now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'Caps are now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('capstriggerratio')) {
            val = parseFloat(argsString);
            if (args.length == 1 || isNaN(val)) {
                $.say($.getWhisperString(sender) + 'The current percentage of caps required to trigger a caps warning is ' + $.ChatModerator.capsratio + '. To change it use: !chatmod capstriggerratio <number between 0.2 and 1.0>');
            } else {
                if (val > 1.0) {
                    val = val / 100;
                } else if (val > 1.0) {
                    val = 1.0;
                } else if (val < 0.2) {
                    val = 0.2;
                }
                $.inidb.set('settings', 'capstriggerratio', val);
                $.ChatModerator.capsratio = val;
                $.say($.getWhisperString(sender) + 'Changed caps warning trigger percentage to ' + val + '%');
            }
        } else if (subCommand.equalsIgnoreCase('capstriggerlength')) {
            val = parseInt(args[1]);
            if (args.length == 1 || isNaN(val)) {
                $.say($.getWhisperString(sender) + 'The current message length required to check for caps is ' + $.ChatModerator.capslimit + '. To change it use: !chatmod capstriggerlength <number greater than 0>');
            } else {
                if (val < 1) {
                    val = 1;
                }
                $.inidb.set('settings', 'capstriggerlength', val);
                $.ChatModerator.capslimit = val;
                $.say($.getWhisperString(sender) + 'Changed caps warning minimum message length to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('ipsallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.ips) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say($.getWhisperString(sender) + 'IP\'s are currently ' + val + '. To change it use: !chatmod ipsallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'ipsallowed', val);
                $.ChatModerator.ips = val;
                if ($.ChatModerator.ips) {
                    $.say($.getWhisperString(sender) + 'IP\'s are now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'IP\'s are now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('ipmessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current ipmessage warning message is ' + $.ChatModerator.ipmessage + '. To change it use: !chatmod ipmessage <any text>');
            } else {
                $.inidb.set('settings', 'ipmessage', val);
                $.ChatModerator.ipmessage = val;
                $.say($.getWhisperString(sender) + 'Changed ipmessage warning message to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('capsmessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current caps warning message is ' + $.ChatModerator.capmessage + '. To change it use: !chatmod capsmessage <any text>');
            } else {
                $.inidb.set('settings', 'capsmessage', val);
                $.ChatModerator.capmessage = val;
                $.say($.getWhisperString(sender) + 'Changed caps warning message to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('linksallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.Links) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say($.getWhisperString(sender) + 'Links are currently ' + val + '. To change it use: !chatmod linksallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'linksallowed', val);
                $.ChatModerator.Links = val;
                if ($.ChatModerator.Links) {
                    $.say($.getWhisperString(sender) + 'Links are now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'Links are now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('permittime')) {
            val = parseInt(args[1]);
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current permit time is ' + $.ChatModerator.permittime + ' seconds. To change it use: !chatmod permittime <number that is at least 60>');
            } else {
                if (val < 60) {
                    val = 60;
                }
                $.inidb.set('settings', 'permittime', val);
                $.ChatModerator.permittime = val;
                $.say($.getWhisperString(sender) + 'Changed permit time to ' + val + ' seconds!');
            }
        } else if (subCommand.equalsIgnoreCase('youtubeallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.youtubelinks) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say('Youtube links are currently ' + val + '. To change it use: !chatmod youtubeallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'youtubeallowed', val);
                $.ChatModerator.youtubelinks = val;
                if ($.ChatModerator.youtubelinks) {
                    $.say($.getWhisperString(sender) + 'Youtube links are now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'Youtube links are now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('subsallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.subscribers) {
                    val = 'allowed';
                } else {
                    val = 'NOT allowed';
                }
                $.say($.getWhisperString(sender) + 'Subscribers are currently ' + val + ' to post links. To change it use: !chatmod subsallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'subsallowed', val);
                $.ChatModerator.subscribers = val;
                if ($.ChatModerator.subscribers) {
                    $.say($.getWhisperString(sender) + 'Subscribers are now allowed to post links!');
                } else {
                    $.say($.getWhisperString(sender) + 'Subscribers are no longer allowed to post links!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('regsallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.regulars) {
                    val = 'allowed';
                } else {
                    val = 'NOT allowed';
                }
                $.say($.getWhisperString(sender) + 'Regulars are currently ' + val + ' to post links. To change it use: !chatmod regsallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'regsallowed', val);
                $.ChatModerator.regulars = val;
                if ($.ChatModerator.regulars) {
                    $.say($.getWhisperString(sender) + 'Regulars are now allowed to post links!');
                } else {
                    $.say($.getWhisperString(sender) + 'Regulars are no longer allowed to post links!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('linksmessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current link warning message is ' + $.ChatModerator.linksmessage + '. To change it use: !chatmod linksmessage <any text>');
            } else {
                $.inidb.set('settings', 'linksmessage', val);
                $.ChatModerator.linksmessage = val;
                $.say($.getWhisperString(sender) + 'Changed link warning message to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('symbolsallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.symbols) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say($.getWhisperString(sender) + 'Symbol spam is currently ' + val + '. To change it use: !chatmod symbolsallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'symbolsallowed', val);
                $.ChatModerator.symbols = val;
                if ($.ChatModerator.symbols) {
                    $.say($.getWhisperString(sender) + 'Symbol spam is now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'Symbol spam is now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('symbolslimit')) {
            val = parseInt(args[1]);
            if (args.length == 1 || isNaN(val)) {
                $.say($.getWhisperString(sender) + 'The current maximum number of symbols allowed in a message is ' + $.ChatModerator.symbolslimit + '. To change it use: !chatmod symbolslimit <number, at least 1>');
            } else {
                if (val < 1) {
                    val = 1;
                }
                $.inidb.set('settings', 'symbolslimit', val);
                $.ChatModerator.symbolslimit = val;
                $.say($.getWhisperString(sender) + 'Changed number of symbols allowed per message to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('symbolsrepeatlimit')) {
            val = argsString2;
            if (args.length == 1 || isNaN(val)) {
                $.say($.getWhisperString(sender) + 'The current maximum repeating symbols sequence allowed in a message is ' + $.ChatModerator.symbolslimit + '. To change it use: !chatmod symbolsrepeatlimit <number, at least 1>');
            } else {
                if (val < 1) {
                    val = 1;
                }
                $.inidb.set('settings', 'symbolsrepeatlimit', val);
                $.ChatModerator.symbolslimit = val;
                $.say($.getWhisperString(sender) + 'Changed maximum repeating symbols sequence allowed in a message to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('symbolsmessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current symbols warning message is ' + $.ChatModerator.symbolsmessage + '. To change it use: !chatmod symbolsmessage <any text>');
            } else {
                $.inidb.set('settings', 'symbolsmessage', val);
                $.ChatModerator.symbolsmessage = val;
                $.say($.getWhisperString(sender) + 'Changed symbols warning message to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('repeatallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.repeat) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say($.getWhisperString(sender) + 'Repeating character spam is currently ' + val + '. To change it use: !chatmod repeatallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'repeatallowed', val);
                $.ChatModerator.repeat = val;
                if ($.ChatModerator.repeat) {
                    $.say($.getWhisperString(sender) + 'Repeating character spam is now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'Repeating character spam is now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('repeatlimit')) {
            val = parseInt(args[1]);
            if (args.length == 1 || isNaN(val)) {
                $.say($.getWhisperString(sender) + 'The maximum number of repeating sequences/repeating sequence length is ' + $.ChatModerator.repeatlimit + '. To change it use: !chatmod repeatlimit <number, at least 1>');
            } else {
                if (val < 1) {
                    val = 1;
                }
                $.inidb.set('settings', 'repeatlimit', val);
                $.ChatModerator.repeatlimit = val;
                $.say($.getWhisperString(sender) + 'Changed maximum number of repeating sequences/repeating sequence length to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('repeatmessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current repeating character warning message is ' + $.ChatModerator.repeatmessage + '. To change it use: !chatmod repeatmessage <any text>');
            } else {
                $.inidb.set('settings', 'repeatmessage', val);
                $.ChatModerator.repeatmessage = val;
                $.say($.getWhisperString(sender) + 'Changed repeating character warning message to ' + val + ' !');
            }
        } else if (subCommand.equalsIgnoreCase('graphemeallowed')) {
            val = argsString2;
            if (args.length == 1 || (!val.equalsIgnoreCase('false') && !val.equalsIgnoreCase('true'))) {
                if ($.ChatModerator.grapheme) {
                    val = 'allowed';
                } else {
                    val = 'moderated';
                }
                $.say($.getWhisperString(sender) + 'Long grapheme clusters are currently ' + val + '. To change it use: !chatmod graphemeallowed <"true" or "false">');
            } else {
                if (val.equalsIgnoreCase('true')) {
                    val = '1';
                } else {
                    val = '0';
                }
                $.inidb.set('settings', 'graphemeallowed', val);
                $.ChatModerator.grapheme = val;
                if ($.ChatModerator.grapheme) {
                    $.say($.getWhisperString(sender) + 'Long grapheme clusters are now allowed!');
                } else {
                    $.say($.getWhisperString(sender) + 'Long grapheme clusters are now moderated!');
                }
            }
        } else if (subCommand.equalsIgnoreCase('graphemelimit')) {
            val = parseInt(args[1]);
            if (args.length == 1 || isNaN(val)) {
                $.say($.getWhisperString(sender) + 'The maximum allowed grapheme cluster length is ' + $.ChatModerator.graphemelimit + '. To change it use: !chatmod graphemelimit <number, at least 1>');
            } else {
                if (val < 1) {
                    val = 1;
                }
                $.inidb.set('settings', 'graphemelimit', val);
                $.ChatModerator.graphemelimit = val;
                $.say($.getWhisperString(sender) + 'Changed maximum allowed grapheme cluster length to ' + val + '!');
            }
        } else if (subCommand.equalsIgnoreCase('graphememessage')) {
            val = argsString2;
            if (args.length == 1) {
                $.say($.getWhisperString(sender) + 'The current long grapheme cluster are warning message is ' + $.ChatModerator.graphememessage + '. To change it use: !chatmod graphememessage <any text>');
            } else {
                $.inidb.set('settings', 'graphememessage', val);
                $.ChatModerator.graphememessage = val;
                $.say($.getWhisperString(sender) + 'Changed long grapheme cluster warning message to ' + val + '!');
            }
        }
    }
});

$.on('ircChannelMessage', function (event) {
    var sender = event.getSender();
    var message = event.getMessage();
    var capcount = event.getCapsCount();
    var capsRatio = capcount / message.length;
    var permitted = false;
    
    for (var i = 0; i < $.ChatModerator.blacklist.length; i++) {
        if (message.toLowerCase().contains($.inidb.get('blacklist', $.ChatModerator.blacklist[i]).toLowerCase()) && !$.isModv3(sender, event.getTags())) {
            $.ChatModerator.timeoutUserFor(sender, 600);
            $.say($.ChatModerator.blacklistMessage + ' (' + sender + ') (timeout)'); 
            $.logEvent('chatModerator.js', 629, 'Blacklist phrase triggered by ' + sender + '. Message: ' + message);
        }
    }

    if (!$.isModv3(sender, event.getTags()) && (!$.isSubv3(sender, event.getTags()) || !$.ChatModerator.subscribers) && (!$.isReg(sender) || !$.ChatModerator.regulars)) {
        for (var i = 0; i < $.ChatModerator.whitelist.length; i++) {
            if (message.toLowerCase().contains($.inidb.get('whitelist', $.ChatModerator.whitelist[i]).toLowerCase())) {
                permitted = true;
                return;
            }
        }
        for (var i in $.ChatModerator.permitList) {
            if ($.ChatModerator.permitList[i].equalsIgnoreCase(sender) && $.hasLinks(event)) {
                $.ChatModerator.permitList.splice(i, 1);
                permitted = true;
                return;
            }
        }
    
        if ($.ChatModerator.youtubelinks && (message.contains('youtube.com') || message.contains('youtu.be'))) {
            permitted = true;
            $.logEvent('chatModerator.js', 647, 'Automatic youtube link permit for ' + sender + '. Link: ' + $.getLastLink() + ' Message: ' + message);
        } else if (!$.ChatModerator.Links && !permitted && $.hasLinks(event)) {
            $.ChatModerator.timeoutUser(sender);
            $.say($.ChatModerator.Linksmessage + ' (' + sender + ') ' + $.ChatModerator.timeoutwarningmsg);
            $.logEvent('chatModerator.js', 652, 'Automatic link punishment triggered by ' + sender + '. Link: ' + $.getLastLink() + ' Message: ' + message);
        } else if (!$.ChatModerator.caps && (capsRatio > $.ChatModerator.capsratio) && (message.length > $.ChatModerator.capslimit)) {
            $.ChatModerator.timeoutUser(sender);
            $.say($.ChatModerator.capsmessage + ' (' + sender + ') ' + $.ChatModerator.timeoutwarningmsg);
            $.logEvent('chatModerator.js', 657, 'Automatic caps punishment triggered by ' + sender + '. Message Length: ' + message.length + ' Caps Ratio: ' + capsRatio + ' Message: ' + message);
        } else if (!$.ChatModerator.ips && $.getIP(event)) {
            $.ChatModerator.timeoutUser(sender);
            $.say($.ChatModerator.ipmessage + ' (' + sender + ') ' + $.ChatModerator.timeoutwarningmsg);
            $.logEvent('chatModerator.js', 662, 'Automatic ip address punishment triggered by ' + sender + '. Message: ' + message);
        } else if (!$.ChatModerator.symbols && ($.getNumberOfNonLetters(event) > $.ChatModerator.symbolslimit || $.getLongestNonLetterSequence(event) > $.ChatModerator.symbolslimit)) {
            $.ChatModerator.timeoutUser(sender);
            $.say($.ChatModerator.symbolsmessage + ' (' + sender + ') ' + $.ChatModerator.timeoutwarningmsg);
            $.logEvent('chatModerator.js', 667, 'Automatic symbols punishment triggered by ' + sender + '. Longest symbol sequence: ' + $.getLongestNonLetterSequence(event) + '. Total symbols: ' + $.getNumberOfNonLetters(event) + '. Message: ' + message);
        } else if (!$.ChatModerator.repeat && ($.getNumberOfRepeatSequences(event) > $.ChatModerator.repeatlimit || $.getLongestRepeatedSequence(event) > $.ChatModerator.repeatlimit) && $.getLongestRepeatedSequence(event)) {
            $.ChatModerator.timeoutUser(sender);
            $.say($.ChatModerator.repeatmessage + ' (' + sender + ') ' + $.ChatModerator.timeoutwarningmsg);
            $.logEvent('chatModerator.js', 672, 'Automatic repeat punishment triggered by ' + sender + '. Longest repeat sequence: ' + $.getLongestRepeatedSequence(event) + '. Total repeat sequences: ' + $.getNumberOfRepeatSequences(event) + '. Message: ' + message);
        } else if (!$.ChatModerator.grapheme && $.getLongestUnicodeGraphemeCluster(event) > $.ChatModerator.graphemelimit) {
            $.ChatModerator.timeoutUser(sender);
            $.say($.ChatModerator.graphememessage + ' (' + sender + ') ' + $.ChatModerator.timeoutwarningmsg);
            $.logEvent('chatModerator.js', 677, 'Automatic grapheme punishment triggered by ' + sender + '. Longest grapheme sequence: ' + $.getLongestUnicodeGraphemeCluster(event) + '. Message: ' + message);
        } 
    }
});

$.registerChatCommand('./util/chatModerator.js', 'chatmod', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'purge', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'timeout', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'ban', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'unban', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'clear', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'permit', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'blacklist', 'mod');
$.registerChatCommand('./util/chatModerator.js', 'whitelist', 'mod');
